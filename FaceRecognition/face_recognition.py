#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  9 12:25:03 2019

@author: lukashubl
"""

import numpy as np
import os
import imageio
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn import preprocessing, decomposition
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.utils import resample
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
import seaborn as sns
from scipy import fftpack
from matplotlib.colors import LogNorm
from PIL import Image
from skimage.filters import prewitt_h,prewitt_v
from skimage.io import imread, imshow

# Function to create a confusion matrix and the corresponding plot
# Parameters:   target: target labels of test data
#               predicted: predicted lables of test data
#               data: original dataset
def create_confusion_matrix_plot(target, predicted,data):
    # create confusion matrix over all involved classes
    cm = confusion_matrix(target, predicted)

    fig, ax = plt.subplots(figsize=(20, 10))
    im = ax.imshow(cm, interpolation='nearest')
    ax.figure.colorbar(im, ax=ax)
    # Show all ticks
    ax.set(xticks=np.arange(cm.shape[1]),
    yticks=np.arange(cm.shape[0]),
    # Show all possible labels
    xticklabels=pd.unique(data.iloc[:,-1]), yticklabels=pd.unique(data.iloc[:,-1]),
    title='Confusion Matrix Face Recognition rbfSVC',
    ylabel='True label',
    xlabel='Predicted label')
    # rotate x labels for better visibility
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
    rotation_mode="anchor")
    plt.show()
    fig.savefig("confusion_matrix.svg")

# Function to create the cummulative variance (pca) and the corresponding plot
# Parameters:   model: the model from the gridsearch (pca fitting needed in pipeline)
#               threshold: for plotting horizontal variance threshold
def create_pca_variance_plot(model, threshold):
    # calculate cummulative variance
    cumsum_best_knn = np.cumsum(best_knn.steps[1][1].explained_variance_ratio_)
    #TODO: what is the steps meaning in detail --> link to description: https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.Pipeline.html
    # plot variances
    fig, (ax1, ax2) = plt.subplots(1,2, figsize=(9, 4)) # create subplot with 1 row and 2 columns
    ax1.scatter(range(0, len(model.steps[1][1].explained_variance_ratio_)), model.steps[1][1].explained_variance_ratio_)
    ax1.set_xlabel('Features')
    ax1.set_ylabel('Variance')
    ax2.scatter(range(0,len(cumsum_best_knn)), cumsum_best_knn)
    ax2.axhline(y = threshold, linewidth=1, color='r')
    ax2.set_xlabel('Features')
    ax2.set_ylabel('Cummulated Variance')

# Function to inverse the pca used to show how much information is lost by clipping
# a certain amount of principal components
# Parameters:   n_components: how much variance should be preserved
#               data_train: dataset to fit the pca 
def reconstruct_pca_principle_components(n_components, model, plot_rows, plot_columns):
    fig, axes = plt.subplots(plot_rows, plot_columns, figsize=(9, 4), subplot_kw={'xticks':[], 'yticks':[]}, gridspec_kw=dict(hspace=0.1, wspace=0.1))
    for i, ax in enumerate(axes.flat):
        if i < n_components:
            ax.imshow(model.steps[1][1].components_[i].reshape(50, 50), cmap='bone')
    
# Function to enhance image variance due to fft transform and crossing of 
# magnitude of one and phase of another image from the same person
# The idea for this variance enhancement was found in the following paper: https://www.atlantis-press.com/proceedings/icmt-13/10493
# Parameters:   data_train: training dataframe without labels
#               target_train: corresponding labels for the training dataframe samples
def fft_feature_enhancement(data_train, target_train):
    new_data_train = data_train.copy(deep = True)
    
    # get the indices of each sample grouped by person within the dataset
    mapping = {}
    for position, target in enumerate(target_train):
        if target in mapping:
            mapping[target].append(position)
        else:
            mapping[target] = [position]
    
    # iterate all classes/persons within the dataset
    for key in mapping.keys():
        position_array = mapping[key]
        
        # iterate all samples per person and cross the fft magnitude of person i 
        # with the fft phase of person i+1
        for ctr in range(len(position_array)):
            # the last element in the list is crossed with the first one (overflow)
            if ctr+1 < len(position_array):
                first_index = ctr
                second_index = ctr + 1
            else:
                first_index = ctr
                second_index = 0
                
            # get image pairs from same category (person)
            original_image = np.reshape(data_train.iloc[position_array[first_index], :].tolist(), [50,50])
            crossing_image = np.reshape(data_train.iloc[position_array[second_index], :].tolist(), [50,50])
            
            # fft of first image (only real)
            original_fft = fftpack.fft2(original_image)
            original_fft_real = original_fft.real
            
            # fft of second image (only imaginary)
            crossing_fft = fftpack.fft2(crossing_image)
            crossing_fft_imag = crossing_fft.imag
            
            crossed_image = np.complex128(np.zeros((50, 50)))
            
            # create the crossed image real + imag from other image
            rows = original_image.shape[0]
            columns = original_image.shape[1]
            for row in range(rows):
                for column in range(columns):
                    crossed_image[row][column] = complex(original_fft_real[row][column], crossing_fft_imag[row][column])
                    
            crossed_image = np.uint8(fftpack.ifft2(crossed_image).real)
            new_data_train.iloc[position_array[first_index]] = pd.Series(crossed_image.flatten())
    return new_data_train
    
# Function to depict the impact if lower frequencies within image are removed
# Parameters:   image: demo image
#               fraction: [0.0, 0.5] where 0.5 is original frequencies           
def frequency_analysis(image, fraction):
    im_fft = fftpack.fft2(image)
    plot_spectrum(im_fft)

    im_fft2 = im_fft.copy()
    r, c = im_fft2.shape
    im_fft2[int(r*fraction):int(r*(1-fraction))] = 0
    im_fft2[:, int(c*fraction):int(c*(1-fraction))] = 0
    
    plt.figure()
    plot_spectrum(im_fft2)
    plt.title('Filtered Spectrum')
    
    plt.figure()
    plot_spectrum(im_fft)
    plt.title('Fourier transform')

    im_new = fftpack.ifft2(im_fft2).real
    
    plt.figure()
    plt.imshow(im_new, plt.cm.gray)
    plt.title('Reconstructed Image')
    

# Frequency plot function
# Parameters:   im_fft: frequency transformed image
def plot_spectrum(im_fft):
    plt.imshow(np.abs(im_fft), norm=LogNorm(vmin=5))
    plt.colorbar()
    
# Function to print the classification report of our model
# Parameters:   target_test: real labels of our test set
#               predicted_faces: predicted labels from model
def create_classification_report(target_test, predicted_faces):
    labels = []
    for i in range(len(target_test.value_counts())): labels.append('Person ' + str(i))
    print(classification_report(target_test, predicted_faces, target_names = labels))
    
def showExampleForImageEdgeDetection():
    # load one image
    image = imread('0.0_00_00_haarcascade_frontalface_alt2_crop1_w999_h1330_face339.png')
    
    # show original
    imshow(image)
    
    #calculating horizontal edges using prewitt kernel
    edges_prewitt_horizontal = prewitt_h(image)
    
    #calculating vertical edges using prewitt kernel
    edges_prewitt_vertical = prewitt_v(image)
    
    # show detected edge images
    imshow(edges_prewitt_horizontal, cmap='gray')
    imshow(edges_prewitt_vertical, cmap='gray')
    
# Function to perform image edge analysis in horizontal and vertical directions.
# Parameters:   data_train: training dataframe without labels
#               target_train: corresponding labels for the training dataframe samples
def performImageEdgeDetection(data_train, target_train, useVerticalEdges):
    
    new_data_train = data_train.copy(deep = True)
    
    # get the indices of each sample grouped by person within the dataset
    # from collections import defaultdict
    mapping = {}
    # imgs = defaultdict(list)
    for position, target in enumerate(target_train):
        if target in mapping:
            mapping[target].append(position)
        else:
            mapping[target] = [position]
    
    # iterate all classes/persons within the dataset
    for key in mapping.keys():
        position_array = mapping[key]
        
        # iterate all samples per person and cross the fft magnitude of person i 
        # with the fft phase of person i+1
        for index in range(len(position_array)):
            original_image = np.reshape(data_train.iloc[position_array[0], :].tolist(), [50,50])
            
            if useVerticalEdges == True:
                edges = prewitt_v(original_image)
            else:
                edges = prewitt_h(original_image)

            # imgs[key].append(edges_prewitt_horizontal)
            new_data_train.iloc[position_array[index]] = pd.Series(edges.flatten())
            
    return new_data_train
    
    
    
# Load the png data from the file provided into a single dataframe
directory_as_str = r"/Users/lukashubl/Desktop/faces"

matrix = []
labels = []
for file in sorted(os.listdir(directory_as_str)):
    if file.endswith('.png'):
        filename_parts = file.split('_')
        labels.append(filename_parts[1])
        im = imageio.imread(str(directory_as_str) + '/' + str(file))
        flat_image = im.flatten()
        matrix.append(flat_image)

faces = pd.DataFrame(matrix)
faces['Label'] = labels

# Taking the first 100 pixels and checking the correlation
plt.figure(figsize=(20,20))
heatmap=sns.heatmap(faces.iloc[:,0:100].corr(),vmin=-1,cmap='coolwarm');
heatmap.get_figure().savefig("correlation_heatmap.svg")

# analyze data set balance
faces['Label'].value_counts()

# Label 14 can be removed because it has not got enough samples
faces= faces[faces['Label'] != '14']


# Encode the string-labels to integers. Needed for some models
le = preprocessing.LabelEncoder()
le.fit(pd.unique(faces.iloc[:,-1]))

# Split up data to have a heldback test set
data_train, data_test, target_train, target_test = train_test_split(faces.iloc[:,0:2500], faces.iloc[:,-1], train_size=0.75,random_state=98463257, stratify=faces.iloc[:,-1])

data_train['Label'] = target_train
maxAmount = target_train.value_counts().max()

# Resampling because of an unbalanced dataset
label_23 = resample(data_train[data_train['Label'] == '23'], replace=True,n_samples=maxAmount,random_state=42)
label_18 = resample(data_train[data_train['Label'] == '18'], replace=True,n_samples=maxAmount,random_state=42)
label_24 = resample(data_train[data_train['Label'] == '24'], replace=True,n_samples=maxAmount,random_state=42)

# Removing old values
data_train.drop(data_train.index[data_train['Label'] == '23'], inplace = True)
data_train.drop(data_train.index[data_train['Label'] == '18'], inplace = True)
data_train.drop(data_train.index[data_train['Label'] == '24'], inplace = True)

# Appending new values
data_train = data_train.append(label_23)
data_train = data_train.append(label_18)
data_train = data_train.append(label_24)

data_train['Label'].value_counts()

target_train = data_train['Label']
data_train.drop(columns = ['Label'], inplace=True)


# trying to enhance features
# data_train = fft_feature_enhancement(data_train, target_train)
# data_train = performImageEdgeDetection(data_train, target_train, True)
# data_train = performImageEdgeDetection(data_train, target_train, False)

# Define all wanted models
names = ["NearestNeighbors", "linearSVC", "rbfSVC", "DecisionTree", "RandomForest"]

number_of_pca_components = 5

# Define the classifiers behind the model names
classifiers = {
    'NearestNeighbors': KNeighborsClassifier(),
    'linearSVC': SVC(kernel='linear', probability = True),
    'rbfSVC': SVC(kernel='rbf'),
    'DecisionTree': DecisionTreeClassifier(),
    'RandomForest': RandomForestClassifier()
}

# Define the tunable hyper parameters for the models
params = {
    'NearestNeighbors': {'n_neighbors': [n for n in range(1,15)]},
    'linearSVC':{'C': [3**n for n in range(-5,5)]},
    'rbfSVC':{'C': [3**n for n in range(-5,5)], 'gamma':[3**n for n in range(-5,5)]},
    'DecisionTree': {'max_depth':[n for n in range(1,10)]},
    'RandomForest':{'max_depth':[n for n in range(1,10)], 'n_estimators':[n for n in range(1,10)]},
}

# Empty Array to insert models afterwards
grid_searches={}

# Make grid search for all defined models and parameter configurations
for name in names:
    print("Running GridSearchCV for %s." % name)
    # Create pipeline with scaling, PCA and classifiert
    pipe = Pipeline([
            # Scale so that mean is 0 and std is 1
            ('normalization', StandardScaler()),
            # Set that number_of_pca_components are kept
            ('pca', decomposition.PCA(n_components=number_of_pca_components)),
            # Set classifier to knearestneighbor
            ('classify', classifiers[name])
    ])
    # Hyperparamter range
    parameters_to_tune = params[name]

    # Translate paramters so that the pipeline can use it
    param_grid = {}
    for key,value in parameters_to_tune.items():
        hyperparam_key = "classify__" + key
        param_grid[hyperparam_key] = value

    # Make Grid Search with cv 10
    gs = GridSearchCV(pipe, param_grid=param_grid, cv=10, scoring='accuracy')
    # Train the models
    gs.fit(data_train, target_train)
    # Store the models in the array
    grid_searches[name] = gs

# Save best estimators
best_knn=grid_searches['NearestNeighbors'].best_estimator_
best_linear_svc=grid_searches['linearSVC'].best_estimator_
best_rbf_svc=grid_searches['rbfSVC'].best_estimator_
best_decision_tree=grid_searches['DecisionTree'].best_estimator_
best_random_forest=grid_searches['RandomForest'].best_estimator_

# check pca config by retransformation
reconstruct_pca_principle_components(number_of_pca_components, best_rbf_svc, 1, 5)
create_pca_variance_plot(best_knn, 0.5)

# Looking at the times of the three similar performances
grid_searches['NearestNeighbors'].cv_results_['mean_fit_time']
grid_searches['rbfSVC'].cv_results_['mean_fit_time']
grid_searches['linearSVC'].cv_results_['mean_fit_time']
grid_searches['RandomForest'].cv_results_['mean_fit_time']

grid_searches['NearestNeighbors'].cv_results_['mean_score_time']
grid_searches['rbfSVC'].cv_results_['mean_score_time']
grid_searches['linearSVC'].cv_results_['mean_score_time']
grid_searches['RandomForest'].cv_results_['mean_score_time']

# Extract scores to plot
results_test_acc = []
results_test_std=[]
labels = []

results_test_acc.append(grid_searches['NearestNeighbors'].best_score_)
results_test_std.append(grid_searches['NearestNeighbors'].cv_results_['std_test_score'][grid_searches['NearestNeighbors'].best_index_])
labels.append("Best NearestNeighbor")

results_test_acc.append(grid_searches['rbfSVC'].best_score_)
results_test_std.append(grid_searches['rbfSVC'].cv_results_['std_test_score'][grid_searches['rbfSVC'].best_index_])
labels.append("Best rbfSVC")

results_test_acc.append(grid_searches['linearSVC'].best_score_)
results_test_std.append(grid_searches['linearSVC'].cv_results_['std_test_score'][grid_searches['linearSVC'].best_index_])
labels.append("Best linearSVC")

results_test_acc.append(grid_searches['DecisionTree'].best_score_)
results_test_std.append(grid_searches['DecisionTree'].cv_results_['std_test_score'][grid_searches['DecisionTree'].best_index_])
labels.append("Best DecisionTree")

results_test_acc.append(grid_searches['RandomForest'].best_score_)
results_test_std.append(grid_searches['RandomForest'].cv_results_['std_test_score'][grid_searches['RandomForest'].best_index_])
labels.append("Best RandomForest")

results_test_acc
results_test_std

# Boxplot algorithm comparison
fig = plt.figure()
plt.figure(figsize=(20,8))
fig.suptitle('Algorithm Comparison - Test Data Accuracy with Standard Deviation')
plt.bar(np.arange(len(results_test_acc)), results_test_acc, yerr=results_test_std)
plt.xticks(np.arange(len(results_test_acc)), labels, rotation='vertical')
plt.ylabel('Accuracy')
plt.show()
plt.figure().savefig("algorithm_comparison.svg")


# Considering the score/accuracy of the models in contrast to PC count, the rbf SVC is the best one
# The best estimators are already retrained with the whole dataset when refit is set to true
best_model=grid_searches['rbfSVC'].best_estimator_
# Predict faces from heldback test dataset with best estimator. This also does the scaling and PCA of the best estimator
predicted_faces = best_model.predict(data_test)
# Calculate accuracy
predicted_accuracy = accuracy_score(target_test, predicted_faces)
# Plot confusion matrix
create_confusion_matrix_plot(target_test,predicted_faces,faces)
# Generate classification report
create_classification_report(target_test, predicted_faces)


