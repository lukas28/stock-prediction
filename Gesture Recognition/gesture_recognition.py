# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 13:13:47 2019

@author: Dominik
"""
#%% ---------------------------------------------------------------------------
# ---------------------------- Imports ----------------------------------------
# -----------------------------------------------------------------------------
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pywt
import seaborn as sns
from enum import Enum
from sklearn import preprocessing, decomposition
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier # multi-layer perceptron network
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
import warnings
from sklearn.decomposition import PCA
warnings.filterwarnings("ignore")

#%% ---------------------------------------------------------------------------
# ------------------------- Constants/Enums -----------------------------------
# -----------------------------------------------------------------------------
# defining paths to the csv files
X_PATH = "wear/raw_data_wear_x.csv"
Y_PATH = "wear/raw_data_wear_y.csv"
Z_PATH = "wear/raw_data_wear_z.csv"

class Model(Enum):
    __order__ = 'NEARESTNEIGHBORS LINEARSVC RBFSVC DECISIONTREE RANDOMFOREST LOGISTICREGRESSION NEURALNETWORK'
    NEARESTNEIGHBORS = 1
    LINEARSVC = 2
    RBFSVC = 3
    DECISIONTREE = 4
    RANDOMFOREST = 5
    LOGISTICREGRESSION = 6
    NEURALNETWORK = 7

#%% ---------------------------------------------------------------------------
# -------------------------- Functions ----------------------------------------
# -----------------------------------------------------------------------------

# Creates a confusion matrix and the corresponding plot
# Parameters:   target: target labels of test data
#               predicted: predicted labels of test data
def create_confusion_matrix_plot(target, predicted):
    # create confusion matrix over all involved classes
    cm = confusion_matrix(target, predicted)

    # create plot
    fig, ax = plt.subplots(figsize=(20, 10))
    sns.heatmap(cm, annot=True, ax = ax); #annot=True to annotate cells
    ax.set_xlabel('Predicted labels'); ax.set_ylabel('True labels');
    ax.set_title('Confusion Matrix');
    labels = target_test.unique()
    ax.xaxis.set_ticklabels(labels); ax.yaxis.set_ticklabels(labels);
    fig.savefig("confusion_matrix.svg")

# Normalizes and interpolates a dataframe with labels AND values
# Parameters:   dataframe: source dataframe to be interpolated
#               length: the length of the dataframe
def normalizeLengthAndInterpolate(dataframe, length):
    normAndInt = normalizeLengthAndInterpolateValueDataframe(dataframe.iloc[:,3:],length)
    return(pd.concat([dataframe.iloc[:,0:3],normAndInt], axis=1,ignore_index=True))

# Normalizes and interpolates a dataframe which only consists of values
# Parameters:   dataframe: source dataframe to be interpolated
#               length: the length of the dataframe
def normalizeLengthAndInterpolateValueDataframe(dataframe, length):
    newDataframe = pd.DataFrame()

    for index, row in dataframe.iterrows():
        row = row.dropna()  # remove NaN values

        interpolate = interp1d(np.arange(0, row.shape[0]), row, kind='cubic')
        row = interpolate(np.arange(0, row.shape[0]-1, (row.shape[0]-1)/length))

        newDataframe = newDataframe.append(pd.Series(row[0:length]), ignore_index=True)
    return newDataframe

# Plots a dataframe with time series
def plotDataFrameTimeSeries(dataframe):
    fig = plt.figure()
    for index, row in dataframe.iterrows():
        plt.plot(row, color='black', alpha=0.05)
    plt.show()

# Returns a dataframe with the length of all series without NaN
def getLengthOfSamples(dataframe):
    newDataframe = pd.DataFrame()
    for index, row in dataframe.iterrows():
        newDataframe = newDataframe.append(pd.Series(row.dropna().shape[0]), ignore_index=True)
    return newDataframe

# Reads in the csv files and concatenates them into a single pandas dataframe
# Each line of these three files corresponds to the same recorded gesture
# Csv file columns are in this order: 1. gesture, 2. participantNr, 3. sampleNr, 4. N acceleration values (different lengths per sample)
# Parameters:   x_path: file path of the csv file which contains recorded data on the x axis
#               y_path: file path of the csv file which contains recorded data on the y axis
 #              z_path: file path of the csv file which contains recorded data on the z axis
def readInCsvFiles(x_path, y_path, z_path):
    # Read data from csv files
    xAxis = pd.read_csv(x_path, header=None)
    yAxis = pd.read_csv(y_path, header=None)
    zAxis = pd.read_csv(z_path, header=None)

    # Retrieve labels to add them later to the combined dataframe
    labels = xAxis.iloc[:, 0:3]

    # return combined dataframe
    return pd.concat([xAxis.iloc[:,3:],yAxis.iloc[:,3:],zAxis.iloc[:,3:]], axis=1, ignore_index=True), xAxis, yAxis, zAxis, labels

# Generates a feature set using the length, median, mad
# and the innerquartile range (IRQ) of the data set
def createFeatureSet_statisticalData(data):
    _, xAxis, yAxis, zAxis, _ = readInCsvFiles(X_PATH, Y_PATH, Z_PATH)

    medianOfXSamples = xAxis.iloc[:,3:].median(axis=1)
    medianOfYSamples = yAxis.iloc[:,3:].median(axis=1)
    medianOfZSamples = zAxis.iloc[:,3:].median(axis=1)
    madOfXSamples    = xAxis.iloc[:,3:].mad(axis=1)
    madOfYSamples    = yAxis.iloc[:,3:].mad(axis=1)
    madOfZSamples    = zAxis.iloc[:,3:].mad(axis=1)
    innerQuartileX   = xAxis.iloc[:,3:].quantile(0.75,axis=1) - xAxis.iloc[:,3:].quantile(0.25, axis=1)
    innerQuartileY   = yAxis.iloc[:,3:].quantile(0.75,axis=1) - yAxis.iloc[:,3:].quantile(0.25, axis=1)
    innerQuartileZ   = zAxis.iloc[:,3:].quantile(0.75,axis=1) - zAxis.iloc[:,3:].quantile(0.25, axis=1)

    dataset = pd.concat([
            medianOfXSamples,
            medianOfYSamples,
            medianOfZSamples,
            madOfXSamples,
            madOfYSamples,
            madOfZSamples,
            innerQuartileX,
            innerQuartileY,
            innerQuartileZ
    ], axis=1, ignore_index=True)

    return dataset

# Generates a feature set using the original data interpolated and
# normalized to a uniform length, also a savgol filter is applied to the dataset
def createFeatureSet_interpolated_normalized_filtered(data):
    # interpolate and normalize data to a uniform length
    samples = normalizeLengthAndInterpolateValueDataframe(data, 60)

    # filter samples and save filtered dataframe
    filteredSamples = savgol_filter(samples, 11, 3) # GREANI why did you use the savitzky golay filter?; GREANI, LUKAS mochts wirklich sinn dass ma immer =d23= bei de ableitungen schreiben? (ma siehts eh nachher in da parameterliste + mi fein de compiler warnings an zwecks unused variable),used savgol because it looked the best, i glaub d23 kannst wegdoa
    newFeatureSet = pd.DataFrame(filteredSamples)

    # plot
    plotDataFrameTimeSeries(newFeatureSet[newFeatureSet[0]=="left"].iloc[:,2:])

    return newFeatureSet


# Creates a feature set using the original data transformed via
# fast fourier transform (fft)
def createFeatureSet_fourierTranformed(data):
    data_scaled = normalizeLengthAndInterpolateValueDataframe(data, 60)

    fft_calc =  np.fft.fft(data_scaled, axis=1)

    # phase
    phase_spectrum = np.angle(fft_calc)

    # power
    power_spectrum=np.abs(fft_calc)**2
    #plt.bar(list(range(len(fft_calc[0]))), power_spectrum[0])

    # combine transformed values into a new dataframe
    #return pd.concat([pd.DataFrame(power_spectrum).iloc[:,0:12], pd.DataFrame(phase_spectrum).iloc[:,0:12]], axis=1, ignore_index=True)
    return pd.concat([pd.DataFrame(power_spectrum).iloc[:,:], pd.DataFrame(phase_spectrum).iloc[:,:]], axis=1, ignore_index=True)




# Creates a feature set based on the autocorrelations of the samples
# Every sample is correlated with itself in the lags from 0 to 60
# Results are returned as a dataframe in the same order as the input data
def createFeatureSet_autocorrelation(data):
    samples = normalizeLengthAndInterpolateValueDataframe(data, 60)
    result = []
    for index, sample in samples.iterrows():
        sample = sample.dropna()    # remove NaN values
        row = []
        for i in range(60):
            row.append(sample.autocorr(lag = i+1))
        result.append(row)
    return pd.DataFrame(result).loc[:, :57]


# Creates a feature set...
def createFeatureSet_waveletComponents(data):
    data_scaled = preprocessing.scale(data)
    plt.plot(data_scaled)

    #plot the wavelet
    [phi, psi, x] = pywt.Wavelet('db3').wavefun(level=1)
    plt.plot(x, psi)

     # DWT: Multilevel 1D Discrete Wavelet Transform of data.
    coeffs = pywt.wavedec(data_scaled, 'db3', level = 4) # db=Daubechie Wavelets, 3 = the specific wavelet number

    #split up the coefficients to the details (one per level) plus the approximation of the coefficients
    (approx_coeff, detail_coeff1, detail_coeff2, detail_coeff3,detail_coeff4) = coeffs

    #detail data for each level
    plt.stem(detail_coeff1); plt.legend(['Lvl 1 detail coefficients'])
    plt.stem(detail_coeff2); plt.legend(['Lvl 2 detail coefficients'])
    plt.stem(detail_coeff3); plt.legend(['Lvl 3 detail coefficients'])
    plt.stem(detail_coeff4); plt.legend(['Lvl 4 detail coefficients'])

    plt.stem(approx_coeff); plt.legend(['Approximation Coefficients']) # ...and these are its obtained features for different levels (=scalings of wavelet)
# wavelets and multi resolution analysis are a very powerful concept:
#   * they capture time and "frequency" information at once
#   * remember them if you need to go deep into time series analysis!


# Creates a feature set with a sliding window over the normalizes and interpolated data and 50% overlap
def createFeatureSet_slidingWindow(data,window_size):
    data_uniform = normalizeLengthAndInterpolateValueDataframe(data, 60)
    sliding_window_mean=data_uniform.rolling(window_size,axis=1).mean().iloc[:,window_size-1:-(window_size)].iloc[:,0::int(window_size/2)]
    sliding_window_std=data_uniform.rolling(window_size,axis=1).std().iloc[:,window_size-1:-(window_size)].iloc[:,0::int(window_size/2)]
    return(pd.concat([sliding_window_mean,sliding_window_std],axis=1,ignore_index=True))

# Visualizes the difference between the raw (unprocessed) and the
# preprocessed data
def showPreprocessingResults():
    results_raw = {}
    results_preprocessed = {}
    results_preprocessed_filtered = {}

    #relevantGestures = ['left', 'right', 'up', 'down']
    relevantGestures = ['left', 'down']

    _, x, y, z = readInCsvFiles(X_PATH, Y_PATH, Z_PATH)

    for gesture in relevantGestures:
        results_raw['x_' + gesture] = x[x[0] == gesture].iloc[:, 4:]
        results_raw['y_' + gesture] = y[y[0] == gesture].iloc[:, 4:]
        results_raw['z_' + gesture] = z[z[0] == gesture].iloc[:, 4:]

        results_preprocessed['x_' + gesture] = normalizeLengthAndInterpolateValueDataframe(x[x[0] == gesture].iloc[:, 4:], 60)
        results_preprocessed['y_' + gesture] = normalizeLengthAndInterpolateValueDataframe(y[y[0] == gesture].iloc[:, 4:], 60)
        results_preprocessed['z_' + gesture] = normalizeLengthAndInterpolateValueDataframe(z[z[0] == gesture].iloc[:, 4:], 60)

        results_preprocessed_filtered['x_' + gesture] = d23 = savgol_filter(results_preprocessed['x_' + gesture], 21, 3)
        results_preprocessed_filtered['x_' + gesture] = pd.DataFrame(results_preprocessed_filtered['x_' + gesture])
        results_preprocessed_filtered['y_' + gesture] = d23 = savgol_filter(results_preprocessed['y_' + gesture], 21, 3)
        results_preprocessed_filtered['y_' + gesture] = pd.DataFrame(results_preprocessed_filtered['y_' + gesture])
        results_preprocessed_filtered['z_' + gesture] = d23 = savgol_filter(results_preprocessed['z_' + gesture], 21, 3)
        results_preprocessed_filtered['z_' + gesture] = pd.DataFrame(results_preprocessed_filtered['z_' + gesture])

    figure_raw = plt.figure(figsize=(25, 4))
    figure_raw.suptitle('Acceleration data of x,y,z axis raw', fontsize=16)
    ctr = 1
    for raw_result in results_raw.values():
        ax = figure_raw.add_subplot(1, 6, ctr)
        ax.set_title(list(results_raw.keys())[ctr - 1])
        ax.set_xlabel('Acceleration features')
        ax.set_ylabel('Acceleration [G]')
        for i in range(len(raw_result)):
            ax.plot(raw_result.iloc[i].tolist(), linewidth=0.1, c = 'black')
        ctr += 1

    figure_processed = plt.figure(figsize=(25, 4))
    figure_processed.suptitle('Acceleration data of x,y,z axis processed', fontsize=16)
    ctr = 1
    for processed_result in results_preprocessed.values():
        ax = figure_processed.add_subplot(1, 6, ctr)
        ax.set_title(list(results_preprocessed.keys())[ctr - 1])
        ax.set_xlabel('Acceleration features')
        ax.set_ylabel('Acceleration [G]')
        for i in range(len(processed_result)):
            ax.plot(processed_result.iloc[i].tolist(), linewidth=0.1, c = 'black')
        ctr += 1

    figure_processed_filter = plt.figure(figsize=(25, 4))
    figure_processed_filter.suptitle('Acceleration data of x,y,z axis processed incl. savgol filter', fontsize=16)
    ctr = 1
    for processed_result in results_preprocessed_filtered.values():
        ax = figure_processed_filter.add_subplot(1, 6, ctr)
        ax.set_title(list(results_preprocessed_filtered.keys())[ctr - 1])
        ax.set_xlabel('Acceleration features')
        ax.set_ylabel('Acceleration [G]')
        for i in range(len(processed_result)):
            ax.plot(processed_result.iloc[i].tolist(), linewidth=0.1, c = 'black')
        ctr += 1

# Prepends the labels to the given dataset
def labelFeatureSet(featureSet, labels):
    return pd.concat([labels,featureSet], axis=1, ignore_index=True)


#%% ---------------------------------------------------------------------------
# ------------------------------ Code- ----------------------------------------
# -----------------------------------------------------------------------------

# Read data and combine it into a single dataframe
data, xAxis, yAxis, zAxis, labels = readInCsvFiles(X_PATH, Y_PATH, Z_PATH)



# ----------------------------------
# Data Preprocessing
# ----------------------------------

# Choose a feature set to be used (feature sets are being created using
# different preprocessing approaches)
allEstimators={}
<<<<<<< HEAD
for featureNumber in [4]:
    
    activeFeatureSet = featureNumber
=======
for featureNumber in range(2):
    
    activeFeatureSet = featureNumber+1
>>>>>>> 2bf81522e5fa4fea77518363e37df20e7a40dbf8
    print("Running feature set nr."+str(activeFeatureSet))

    
    if (activeFeatureSet == 1):
        featureSet = createFeatureSet_statisticalData(data)
    elif (activeFeatureSet == 2):
        featureSet = createFeatureSet_interpolated_normalized_filtered(data)
    elif (activeFeatureSet == 3):
        featureSet = createFeatureSet_fourierTranformed(data)
    elif (activeFeatureSet == 4):
        featureSet = createFeatureSet_autocorrelation(data)
    elif (activeFeatureSet == 5):
        featureSet = createFeatureSet_slidingWindow(data,10)
    
    # Plot the features in a pairplot to check if the classes are distinguishable
    #sns_plot=sns.pairplot(featureSet1,diag_kind='kde',hue=0)
    #sns_plot.savefig("feature_plot.svg")
    
    
    # Generate dataset depending on feature set
    #dataset=pd.concat([xAxis.iloc[:,0:3],featureSet2],axis=1,ignore_index=True)
    fullDataset = labelFeatureSet(featureSet, labels)
    
    
    # Create train test split
    # The heldback test set has to be done before the independent gallery test set 
    dataset, data_test, target_train_set, target_test = train_test_split(fullDataset.iloc[:,:], fullDataset.iloc[:,0], train_size=0.75, random_state=98463257, stratify=fullDataset.iloc[:,0])
    data_test=data_test.iloc[:,3:]
    
    estimators={}
    independent_scores={}
    for model in Model:
        independent_scores[model]=[]
    
    persons=pd.unique(fullDataset[1])
    
    for person in persons:
        print("Running LSOCV for Person"+str(person))
    
        gallery_independent_test_set = dataset[dataset[1]==person]
        target_train = target_train_set[dataset[1]!=person]
        data_train = dataset[dataset[1]!=person].iloc[:,3:]
        
        # Encode the string-labels to integers - needed for some models
        le = preprocessing.LabelEncoder()
        le.fit(pd.unique(dataset.iloc[:,-1]))
                
        
        # ----------------------------------
        # Principal Component Analysis (PCA)
        # ----------------------------------
        
        number_of_pca_components = 10
        
        # Define the classifiers behind the model models
        classifiers = {
            Model.NEARESTNEIGHBORS:     KNeighborsClassifier(),
            Model.LINEARSVC:            SVC(kernel='linear', probability = True),
            Model.RBFSVC:               SVC(kernel='rbf'),
            Model.DECISIONTREE:         DecisionTreeClassifier(),
            Model.RANDOMFOREST:         RandomForestClassifier(),
            Model.LOGISTICREGRESSION:   LogisticRegression(solver='lbfgs'),
            Model.NEURALNETWORK:        MLPClassifier(solver='adam', alpha=1e-5, random_state=1)
        }
        
        # Define the tunable hyper parameters for the models
        params = {
            Model.NEARESTNEIGHBORS:     {'n_neighbors': [n for n in range(1,16)]},
            Model.LINEARSVC:            {'C': [3**n for n in range(-5,6)]},
            Model.RBFSVC:               {'C': [3**n for n in range(-3,4)], 'gamma':[3**n for n in range(-3,4)]},
            Model.DECISIONTREE:         {'max_depth': [n for n in range(1,11)]},
            Model.RANDOMFOREST:         {'max_depth': [n for n in range(1,11)], 'n_estimators':[n for n in range(1,11)]},
            Model.LOGISTICREGRESSION:   {"C": np.logspace(-3,3,7), "penalty":["l2"]},
            Model.NEURALNETWORK:        {'hidden_layer_sizes': [(10), (10,5), (50,10,5)], 'max_iter': [5,10,25,50,75,100,150,200] }
        }
        
        # Empty Array to insert models afterwards
        grid_searches = {}
        
        #modelsToIgnore = [Model.NEARESTNEIGHBORS,Model.RBFSVC,Model.DECISIONTREE,Model.RANDOMFOREST,Model.LOGISTICREGRESSION,Model.NEURALNETWORK]
        modelsToIgnore=[]
        # Perform a grid search for all defined models and parameter configurations
        for model in Model:
        
            # Conditional used for testing: Deactivates all models within the array
            if (model in modelsToIgnore):
                continue
        
            print("Running GridSearchCV for %s." % model)
        
            # Create pipeline with scaling, PCA and classifiers
            pipe = Pipeline([
                    # Scale so that mean is 0 and std is 1
                    ('normalization', StandardScaler()),
        
                    # Set that number_of_pca_components are kept
                    #('pca', decomposition.PCA(n_components=number_of_pca_components)),
        
                    # Set classifier to the corresponding model
                    ('classify', classifiers[model])
            ])
        
            # Hyperparameter range
            parameters_to_tune = params[model]
        
            # Translate paramters so that the pipeline can use it
            param_grid = {}
            for key,value in parameters_to_tune.items():
                hyperparam_key = "classify__" + key
                param_grid[hyperparam_key] = value
        
            # Make Grid Search with cv 10
            gs = GridSearchCV(pipe, param_grid=param_grid, cv=10, scoring='accuracy')
        
            # Train the models
            gs.fit(data_train, target_train)
        
            # Store the models in the array
            grid_searches[model] = gs
        
        
        
        
        
        # ----------------------------------
        # Evaluate model performance
        # ----------------------------------
        print("\n\n########### Model Performance Evaluation ##########\n")
        for model in Model:
            # Conditional used for testing: Deactivates all models within the array
            if (model in modelsToIgnore):
                continue
        
            best_estimator = grid_searches[model].best_estimator_
        
            # Predict faces from heldback test dataset with best estimator. This also does the scaling and PCA of the best estimator
            predicted_gestures = best_estimator.predict(data_test)
            
            gi_predicted_gestures = best_estimator.predict(gallery_independent_test_set.iloc[:,3:])
            gi_predicted_accuracy = accuracy_score(gallery_independent_test_set.iloc[:,0], gi_predicted_gestures)
        
            independent_scores[model].append(gi_predicted_accuracy)
            # Calculate accuracy and print it
            print("---------------------------------")
            print("Model: ", model)
            print("Predicted Accuracy: ", accuracy_score(target_test, predicted_gestures))
            print("Best score: " + str(grid_searches[model].best_score_))
            print("Best params: " + str(grid_searches[model].best_params_))
            print("Gallerx independent score: "+ str(gi_predicted_accuracy))
            print("---------------------------------\n")
        print("###################################################")
        
        estimators[person]=grid_searches
        
        #HUBL, PINSKER we need to train the best model again including the gallery independent dataset
        
        estimators
    allEstimators[featureNumber]=estimators

estimatorIndex=4
scores=[]
best_scores=[]
independent_array=[]
for model in Model:
    scores.append([])
    best_scores.append([])
    independent_array.append(independent_scores[model])

    
for person in persons:
    for model in Model:
        if (model in modelsToIgnore):
                continue
        scores[model.value-1].extend(allEstimators[estimatorIndex][person][model].cv_results_['mean_test_score'])

        for splitIndex in range(10):
            best_scores[model.value-1].append(allEstimators[estimatorIndex][person][model].cv_results_["split"+str(splitIndex)+"_test_score"][allEstimators[estimatorIndex][person][model].best_index_])

fig = plt.figure(figsize=(15, 6))
fig.suptitle('Boxplot of all grid search scores per model type', fontsize=16)
plt.boxplot(scores)
plt.ylabel('Score')
plt.xlabel('Model Type')
plt.xticks(np.arange(8), ('', 'Nearest Neighbour', 'linearSVC', 'rbfSVC', 'Decision Tree', 'Random Forest', 'Logistic Regression', 'Neural Network'))
fig.savefig("featureset_3_scores.svg")

fig = plt.figure(figsize=(15, 6))
fig.suptitle('Boxplot of best model scores of grid search', fontsize=16)
plt.boxplot(best_scores)
plt.ylabel('Score')
plt.xlabel('Model Type')
plt.xticks(np.arange(8), ('', 'Nearest Neighbour', 'linearSVC', 'rbfSVC', 'Decision Tree', 'Random Forest', 'Logistic Regression', 'Neural Network'))
fig.savefig("featureset_3_best_scores_scores.svg")


fig = plt.figure(figsize=(15, 6))
fig.suptitle('Gallery independent scores of the best estimators per model', fontsize=16)
plt.boxplot(independent_array)
plt.ylabel('Score')
plt.xlabel('Model Type')
plt.xticks(np.arange(8), ('', 'Nearest Neighbour', 'linearSVC', 'rbfSVC', 'Decision Tree', 'Random Forest', 'Logistic Regression', 'Neural Network'))
plt.savefig("featureset_3_independent_scores.svg")

#Refitting with the gallery independent dataset
best_model=grid_searches[Model.NEARESTNEIGHBORS].best_estimator_
best_model.fit(dataset.iloc[:,3:],target_train_set)
heldback_predicted_gestures = best_model.predict(data_test)
create_confusion_matrix_plot(target_test,heldback_predicted_gestures)