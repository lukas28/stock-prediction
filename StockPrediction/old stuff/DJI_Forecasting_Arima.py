#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 10:58:29 2019

@author: lukashubl
"""

import pandas as pd
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
import matplotlib.pyplot as plt
plt.rcParams.update({'figure.figsize':(9,3), 'figure.dpi':120})

# Import data
df = pd.read_csv('data/big/DJI.csv')
df = df[:500]

fig, axes = plt.subplots(1, 2, sharex=True)
axes[0].plot(pd.DataFrame(df['Open'].values).diff())
axes[0].set_title('1st Differencing')
axes[1].set(ylim=(0,1.2))
plot_acf(pd.DataFrame(df['Open'].values).diff().dropna(), ax=axes[1])

plt.show()


from statsmodels.tsa.arima_model import ARIMA

# 1,1,2 ARIMA Model
model = ARIMA(df['Open'].values, order=(1,1,1))
model_fit = model.fit(disp=0)
print(model_fit.summary())


# Plot residual errors
'''residuals = pd.DataFrame(model_fit.resid)
fig, ax = plt.subplots(1,2)
residuals.plot(title="Residuals", ax=ax[0])
residuals.plot(kind='kde', title='Density', ax=ax[1])
plt.show()'''

# Actual vs Fitted
model_fit.plot_predict(dynamic=False)
plt.show()












from statsmodels.tsa.stattools import acf

# Create Training and Test
train = df['Open'].values[:400]
test = df['Open'].values[400:]


# Build Model
#model = ARIMA(train, order=(3,2,1))
model = ARIMA(train, order=(1, 1, 1))
fitted = model.fit(disp=-1)

fitted.plot_predict(dynamic=False)
plt.show()

# Forecast
fc, se, conf = fitted.forecast(30, alpha=0.05)  # 95% conf

plt.plot(fc)
plt.plot(test)
plt.show()

# Make as pandas series
'''fc_series = pd.Series(fc, index=test.index)
lower_series = pd.Series(conf[:, 0], index=test.index)
upper_series = pd.Series(conf[:, 1], index=test.index)'''

# Plot
'''plt.figure(figsize=(12,5), dpi=100)
plt.plot(train, label='training')
plt.plot(test, label='actual')
plt.plot(fc_series, label='forecast')
plt.fill_between(lower_series.index, lower_series, upper_series,
                 color='k', alpha=.15)
plt.title('Forecast vs Actuals')
plt.legend(loc='upper left', fontsize=8)
plt.show()'''
