from statsmodels.tsa.stattools import adfuller
from numpy import log
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from pmdarima.arima.utils import ndiffs
from statsmodels.tsa.arima_model import ARIMA
from Bots.SimpleBot import SimpleBot

tradingBot = SimpleBot()

# LOADING THE DATASET
'''df = pd.read_csv('data/DJI.csv')
full = df[-180:]
full = full.reset_index()


df = df[-180:-100]
#test = df[-160:-155]

df = df.reset_index()
result = adfuller(df['Open'])
#result = adfuller(df.Open.dropna())
print('ADF Statistic: %f' % result[0])
print('p-Open: %f' % result[1])


# PLOT THE TIME SERIES AND CHECK WHICH OF THEM SEEMS STATIONARY
# FOR ME THE FIRST DIFF LOOKS ALREADY PRETTY GOOD
# Original Series
fig, axes = plt.subplots(3, 2, sharex=True)
axes[0, 0].plot(df.Open); axes[0, 0].set_title('Original Series')
plot_acf(df.Open, ax=axes[0, 1])

# 1st Differencing
axes[1, 0].plot(df.Open.diff()); axes[1, 0].set_title('1st Order Differencing')
plot_acf(df.Open.diff().dropna(), ax=axes[1, 1])

# 2nd Differencing
axes[2, 0].plot(df.Open.diff().diff()); axes[2, 0].set_title('2nd Order Differencing')
plot_acf(df.Open.diff().diff().dropna(), ax=axes[2, 1])

#plt.show()

# USE THIS LIBRARY TO ESTIMATE THE NEEDED DEGREE OF DIFFERENTIATION
# THE LAST TO SUGGEST ALSO 1 WHICH CONFIRMS MY THOUGHTS

## Adf Test
print(ndiffs(df.Open, test='adf'))  # 0

# KPSS test
print(ndiffs(df.Open, test='kpss'))  # 1

# PP test:
print(ndiffs(df.Open, test='pp'))  # 1


# TRY TO FIND THE NEEDED P VALUE - THIS IS ACTUALLY PRETTY COMPLEX AND I CANT
# GET IT YET
# PACF plot of 1st differenced series
plt.rcParams.update({'figure.figsize':(9,3), 'figure.dpi':120})

fig, axes = plt.subplots(1, 2, sharex=True)
axes[0].plot(df.Open.diff()); axes[0].set_title('1st Differencing')
axes[1].set(ylim=(0,5))
plot_pacf(df.Open.diff().dropna(), ax=axes[1])

#plt.show()
# the result of this is that p should be 1 because it is the last one over
# blue area in the


# FIND OUT THE NUMBER OF MA TERMS (Q)
fig, axes = plt.subplots(1, 2, sharex=True)
axes[0].plot(df.Open.diff()); axes[0].set_title('1st Differencing')
axes[1].set(ylim=(0,1.2))
plot_acf(df.Open.diff().dropna(), ax=axes[1])

plt.show()
# q should also be 1 here



model = ARIMA(df.Open, order=(1,1,1))
model_fit = model.fit(disp=0)
print(model_fit.summary())


# Plot residual errors
residuals = pd.DataFrame(model_fit.resid)
fig, ax = plt.subplots(1,2)
residuals.plot(title="Residuals", ax=ax[0])
residuals.plot(kind='kde', title='Density', ax=ax[1])
plt.show()

# Actual vs Fitted
model_fit.plot_predict(dynamic=False)
plt.show()'''










'''from statsmodels.tsa.stattools import acf

# Create Training and Test
train = df.Open[:100]
test = df.Open[100:125]


# Build Model
# model = ARIMA(train, order=(3,2,1))
model = ARIMA(train, order=(3, 2, 1))
fitted = model.fit(disp=-1)

# Forecast
fc, se, conf = fitted.forecast(25, alpha=0.05)  # 95% conf

# Make as pandas series
fc_series = pd.Series(fc, index=test.index)
lower_series = pd.Series(conf[:, 0], index=test.index)
upper_series = pd.Series(conf[:, 1], index=test.index)

# Plot
plt.figure(figsize=(12,5), dpi=100)
plt.plot(train, label='training')
plt.plot(test, label='actual')
plt.plot(fc_series, label='forecast')
plt.fill_between(lower_series.index, lower_series, upper_series,
                 color='k', alpha=.15)
plt.title('Forecast vs Actuals')
plt.legend(loc='upper left', fontsize=8)
plt.show()'''


from statsmodels.tsa.arima_model import ARIMA
import pmdarima as pm
import numpy as np


df = pd.read_csv('data/DJI.csv')
full = df[-280:]
full = full.reset_index()

buy = []
sell = []
index_buy = []

for i in range(20):

    df = pd.read_csv('data/DJI.csv')
    full = df[-280:]
    full = full.reset_index()

    df = df[-280+i*5:-180+i*5]
    #test = df[-160:-155]
    #print("geht")

    df = df.reset_index()

    #df = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/wwwusage.csv', names=['value'], header=0)

    model = pm.auto_arima(df.Open, start_p=1, start_q=1,
                          test='adf',       # use adftest to find optimal 'd'
                          max_p=3, max_q=3, # maximum p and q
                          m=1,              # frequency of series
                          d=None,           # let model determine 'd'
                          seasonal=False,   # No Seasonality
                          start_P=0,
                          D=0,
                          trace=True,
                          error_action='ignore',
                          suppress_warnings=True,
                          stepwise=True)

    #print(model.summary())

    #model.plot_diagnostics(figsize=(7,5))
    #plt.show()

    # Forecast
    n_periods = 5
    fc, confint = model.predict(n_periods=n_periods, return_conf_int=True)
    index_of_fc = np.arange(len(df.Open), len(df.Open)+n_periods)

    #print('actual: ' + str(df['Open'].iloc[-1]))
    #print('predicted: ' + str(fc[-1]))

    if fc[-1] > fc[0]:
        print("kaufen")
        buy.append(df.Open.iloc[-1])
        #buy.append(0)
        index_buy.append(99 + i * 5)
    else:
        print("verkaufen")

    # make series for plotting purpose
    fc_series = pd.Series(fc, index=index_of_fc)
    lower_series = pd.Series(confint[:, 0], index=index_of_fc)
    upper_series = pd.Series(confint[:, 1], index=index_of_fc)

    # Plot
    '''plt.plot(df.Open)
    plt.plot(fc_series, color='darkgreen')
    plt.plot(full.Open)
    plt.fill_between(lower_series.index,
                     lower_series,
                     upper_series,
                     color='k', alpha=.15)

    plt.title("Final Forecast of WWW Usage")
    plt.show()'''
plt.plot(full.Open)
for ind, item in enumerate(buy):
    if item > 0:
        plt.plot(index_buy[ind],item,'go')
plt.show()
