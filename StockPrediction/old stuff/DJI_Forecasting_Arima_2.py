#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 11:30:43 2019

@author: lukashubl
"""

from statsmodels.tsa.stattools import acf, pacf
from scipy.ndimage.interpolation import shift
from statsmodels.tsa.arima_model import ARIMA
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


df = pd.read_csv('data/big/DJI.csv')
flat_train_x = df['Open'].values[:4000]
test_y = df['Open'].values[4000:4500]

n_diff = 2
train_x_diff = flat_train_x - shift(flat_train_x, n_diff, cval=np.nan)
train_x_diff = train_x_diff[n_diff: ]
plt.plot(train_x_diff)


lag_acf = acf(train_x_diff, nlags=10)
lag_pacf = pacf(train_x_diff, nlags=10, method='ols')
plt.figure(figsize=(20,5))
plt.subplot(121)
plt.plot(lag_acf)
plt.axhline(y=0,linestyle='--',color='gray')
plt.axhline(y=-1.96/np.sqrt(len(flat_train_x)),linestyle='--',color='gray')
plt.axhline(y=1.96/np.sqrt(len(flat_train_x)),linestyle='--',color='gray')
plt.title('Autocorrelation Function')
#Plot PACF:
plt.subplot(122)
plt.plot(lag_pacf)
plt.axhline(y=0,linestyle='--',color='gray')
plt.axhline(y=-1.96/np.sqrt(len(flat_train_x)),linestyle='--',color='gray')
plt.axhline(y=1.96/np.sqrt(len(flat_train_x)),linestyle='--',color='gray')
plt.title('Partial Autocorrelation Function')
plt.tight_layout()


model = ARIMA(flat_train_x, order=(2, 2, 2))
results_AR = model.fit()
plt.figure(figsize=(20,5))
plt.plot(train_x_diff)
plt.plot(results_AR.fittedvalues, color='red')


plt.figure(figsize=(20,5))
plt.plot(train_x_diff)
plt.plot(-1*results_AR.fittedvalues, color='red')


y_pred = results_AR.predict(start=398, end=597)
y_pred = -1 * y_pred
for i in range(2, 200):
    y_pred[i] = y_pred[i] + test_y[i-2][0]


plt.figure(figsize=(20,5))
plt.plot(y_pred[2:], label='Predictions')
#plt.plot(test_y.flatten()[2:], label='actual')
#plt.plot(shift(test_y.flatten()[2:], 2, cval=np.nan), label='actual')
plt.legend()
plt.show()
