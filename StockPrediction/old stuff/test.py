import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import lag_plot
from pandas import datetime
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
from Bots.SimpleBot import SimpleBot

from statsmodels.tsa.arima_model import ARIMA
import pmdarima as pm
import numpy as np

df = pd.read_csv("data/big/DJI.csv").fillna(0)

df = df[-4000:-2000]
df = df.reset_index()
df.head()















'''plt.figure(figsize=(10,10))
lag_plot(df['Open'], lag=5)
plt.title('Microsoft Autocorrelation plot')'''


train_data, test_data = df[0:int(len(df)*0.8)], df[int(len(df)*0.8):]
'''plt.figure(figsize=(12,7))
plt.title('Microsoft Prices')
plt.xlabel('Dates')
plt.ylabel('Prices')
plt.plot(df['Open'], 'blue', label='Training Data')
plt.plot(test_data['Open'], 'green', label='Testing Data')
plt.xticks(np.arange(0,7982, 1300), df['Date'][0:7982:1300])
plt.legend()'''

def smape_kun(y_true, y_pred):
    return np.mean((np.abs(y_pred - y_true) * 200/ (np.abs(y_pred) +       np.abs(y_true))))

tradingBot = SimpleBot()

train_ar = train_data['Open'].values
test_ar = test_data['Open'].values
history = [x for x in train_ar[-50:]]
startCap = history[-1]
print(type(history))
predictions = list()
buyPointsX = []
buyPointsY = []
sellPointsY = []
for t in range(len(test_ar)):
    model = ARIMA(history, order=(2,2,0))   #220
    model_fit = model.fit(disp=0)

    '''model_fit = pm.auto_arima(history, start_p=1, start_q=1,
                          test='adf',       # use adftest to find optimal 'd'
                          max_p=3, max_q=3, # maximum p and q
                          m=1,              # frequency of series
                          d=None,           # let model determine 'd'
                          seasonal=False,   # No Seasonality
                          start_P=0,
                          D=0,
                          trace=True,
                          error_action='ignore',
                          suppress_warnings=True,
                          stepwise=True)

    output, confint = model_fit.predict(n_periods=4, return_conf_int=True)'''

    output = model_fit.forecast(steps = 4)  #4
    output = output[0]
    print('currentValue: ' + str(history[-1]))
    print('predictedValue: ' + str(output[0]))

    #yhat = output[0][-1]
    yhat = output[-1]
    rise_fall = 1
    if yhat < history[-1]:
        rise_fall = 0
    action = tradingBot.trade(history[-1], rise_fall)




    if action == 1:
        #bought
        buyPointsX.append(t+len(train_data))
        print("appending")
        buyPointsY.append(history[-1])
        sellPointsY.append(0)
    elif action == 0:
        sellPointsY.append(history[-1])
        buyPointsY.append(0)
    else:
        buyPointsY.append(0)
        sellPointsY.append(0)
    predictions.append(yhat)
    obs = test_ar[t]
    history.append(obs)
    history.pop(0)
    print('history length: ' + str(len(history)))
error = mean_squared_error(test_ar, predictions)
print('Testing Mean Squared Error: %.3f' % error)
error2 = smape_kun(test_ar, predictions)
print('Symmetric mean absolute percentage error: %.3f' % error2)

endCap = history[-1]
print('stock trace: ' + str((endCap * 100)/startCap))
print('roi: ' + str(tradingBot.getRoi()))

#pred = [0,0,0,0]
#pred.extend(predictions)
#predictions = pred[:-4]

td = test_data['Open']

plt.figure()
plt.plot(df['Open'], 'green', color='blue', label='Training Data')
plt.plot(test_data.index, predictions, color='orange',
         label='Predicted Price')
plt.plot(test_data.index, test_data['Open'], label='Actual Price')
for ind, item in enumerate(sellPointsY):
    if item > 0:
        plt.plot(test_data.index[ind-1],item,'ro')
for ind, item in enumerate(buyPointsY):
    if item > 0:
        plt.plot(test_data.index[ind-1],item,'go')


plt.title('Microsoft Prices Prediction')
plt.xlabel('Dates')
plt.ylabel('Prices')
#plt.xticks(np.arange(0,7982, 1300), df['Date'][0:7982:1300])
plt.legend()
plt.show()
