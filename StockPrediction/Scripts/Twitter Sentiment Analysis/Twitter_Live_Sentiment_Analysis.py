# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 22:25:19 2019

@author: hpins
"""

from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import json
import SentimentModule as sentmod

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
import time

ACCESS_TOKEN = "1206324961671106562-3QtjkufzSjOxxg6m8rRuzCGnEKuguJ"
ACCESS_TOKEN_SECRET = "QOZeXheN0UAp6whaaCv4ZuctkNGA05tQQvL0qlorZ1oHr"
CONSUMER_KEY = "aAdoaObmeyPSEw8obHWGOAd3Y"
CONSUMER_SECRET = "n4o6hoRgSpJuEBl0exSIGBEA5J799lqv0vtRpCGtZeQQTP5hy5"


class listener(StreamListener):

    def on_data(self, data):
        print("\n")
        print("######################################################")
        print("                    next tweet                        ")
        print("######################################################")
        print("\n")
        
        all_data = json.loads(data)
        
        tweet = all_data["text"]
#        print(tweet)
        
        sentiment_value, confidence = sentmod.sentiment(tweet)
        print(tweet)
        print("Sentiment & Confidence:")
        print(sentiment_value, confidence)
        
        if confidence >= 0.75:
            file = open("twitter-out.txt","a") # a = append
            file.write(sentiment_value)
            file.write('\n')
            file.close()
        
        return True

    def on_error(self, status):
        print(status)

auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

twitterStream = Stream(auth, listener())
twitterStream.filter(track=["dow jones", "nyse", "stock market", "stocks", "DJIA"])


##### Uncomment the code below to generate a live plot of the log file: ######
style.use("ggplot")

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)

def animate(i):
    pullData = open("twitter-out.txt","r").read()
    lines = pullData.split('\n')

    xar = []
    yar = []

    x = 0
    y = 0

    for l in lines:  # for a live graph use: for l in lines[-200:]:
        x += 1
        if "pos" in l:
            y += 1
        elif "neg" in l:
            y -= 0.5    # use a different subtracting factor to account for negativity bias

        xar.append(x)
        yar.append(y)
        
    ax1.clear()
    ax1.plot(xar,yar)
ani = animation.FuncAnimation(fig, animate, interval=1000)
plt.show()
fig.savefig('plot.png')