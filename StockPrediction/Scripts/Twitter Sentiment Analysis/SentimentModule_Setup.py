# -*- coding: utf-8 -*-
"""
Created on Sat Dec 7 20:58:36 2019

@author: hpins
"""

import nltk
import random
from nltk.tokenize import word_tokenize
from nltk.classify.scikitlearn import SklearnClassifier
import pickle

from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC

from nltk.classify import ClassifierI
from statistics import mode


DATASET_NEGATIVE_PATH = "../../data/Sentiment/movie_reviews_negative.txt"
DATASET_POSITIVE_PATH = "../../data/Sentiment/movie_reviews_positive.txt"





class VoteClassifier(ClassifierI):
    def __init__(self, *classifiers):
        self._classifiers = classifiers

    def classify(self, features):
        votes = []
        
        for classifier in self._classifiers:
            vote = classifier.classify(features)
            votes.append(vote)
        return mode(votes)

    def confidence(self, features):
        votes = []
        
        for classifier in self._classifiers:
            vote = classifier.classify(features)
            votes.append(vote)

        chosen_votes_count = votes.count(mode(votes))
        confidence_level = chosen_votes_count / len(votes)
        return confidence_level
        
    
    
    
    
dataset_pos = open(DATASET_POSITIVE_PATH,"r").read()
dataset_neg = open(DATASET_NEGATIVE_PATH,"r").read()

all_words = []
documents = []



#  j = adjective, r = adverb, v = verb
#allowed_word_types = ["J","R","V"]
allowed_word_types = ["J"]

for sentence in dataset_pos.split('\n'):
    documents.append( (sentence, "pos") )
    words = word_tokenize(sentence)
    
    # pos = part of speech
    pos = nltk.pos_tag(words)
    for w in pos:
        # w[1][0] only gets the base word type
        if w[1][0] in allowed_word_types:
            all_words.append(w[0].lower())

    
for sentence in dataset_neg.split('\n'):
    documents.append( (sentence, "neg") )
    words = word_tokenize(sentence)
    
    pos = nltk.pos_tag(words)
    for w in pos:
        if w[1][0] in allowed_word_types:
            all_words.append(w[0].lower())


# save as pickle (wb option = write bytes)
save_documents = open("pickles/documents.pickle", "wb")         # open file
pickle.dump(documents, save_documents)                          # write to file
save_documents.close()                                          # close file again






all_words = nltk.FreqDist(all_words)

word_features = list(all_words.keys())[:5000]


save_word_features = open("pickles/word_features.pickle","wb")
pickle.dump(word_features, save_word_features)
save_word_features.close()



def find_features(document):
    words = word_tokenize(document)
    features = {}
    for w in word_features:
        features[w] = (w in words)

    return features



featuresets = [(find_features(rev), category) for (rev, category) in documents]

save_feature_sets = open("pickles/featuresets.pickle","wb")
pickle.dump(featuresets, save_feature_sets)
save_word_features.close()

# shuffle in order to place feature sets randomly
random.shuffle(featuresets)


# create training and test split:      
training_set = featuresets[:10000]
testing_set =  featuresets[10000:]




NaiveBayes_classifier = nltk.NaiveBayesClassifier.train(training_set)
print("Original Naive Bayes Algo accuracy percent:", (nltk.classify.accuracy(NaiveBayes_classifier, testing_set))*100)
NaiveBayes_classifier.show_most_informative_features(15)

save_classifier = open("pickles/NaiveBayes_classifier.pickle","wb")
pickle.dump(NaiveBayes_classifier, save_classifier)
save_classifier.close()




MNB_classifier = SklearnClassifier(MultinomialNB())
MNB_classifier.train(training_set)
print("MNB_classifier accuracy percent:", (nltk.classify.accuracy(MNB_classifier, testing_set))*100)

save_classifier = open("pickles/MNB_classifier.pickle","wb")
pickle.dump(MNB_classifier, save_classifier)
save_classifier.close()



BernoulliNB_classifier = SklearnClassifier(BernoulliNB())
BernoulliNB_classifier.train(training_set)
print("BernoulliNB_classifier accuracy percent:", (nltk.classify.accuracy(BernoulliNB_classifier, testing_set))*100)

save_classifier = open("pickles/BernoulliNB_classifier.pickle","wb")
pickle.dump(BernoulliNB_classifier, save_classifier)
save_classifier.close()



LogisticRegression_classifier = SklearnClassifier(LogisticRegression())
LogisticRegression_classifier.train(training_set)
print("LogisticRegression_classifier accuracy percent:", (nltk.classify.accuracy(LogisticRegression_classifier, testing_set))*100)

save_classifier = open("pickles/LogisticRegression_classifier.pickle","wb")
pickle.dump(LogisticRegression_classifier, save_classifier)
save_classifier.close()



SGDC_classifier = SklearnClassifier(SGDClassifier())
SGDC_classifier.train(training_set)
print("SGDClassifier accuracy percent:",nltk.classify.accuracy(SGDC_classifier, testing_set)*100)

save_classifier = open("pickles/SGDC_classifier.pickle","wb")
pickle.dump(SGDC_classifier, save_classifier)
save_classifier.close()



##SVC_classifier = SklearnClassifier(SVC())
##SVC_classifier.train(training_set)
##print("SVC_classifier accuracy percent:", (nltk.classify.accuracy(SVC_classifier, testing_set))*100)




LinearSVC_classifier = SklearnClassifier(LinearSVC())
LinearSVC_classifier.train(training_set)
print("LinearSVC_classifier accuracy percent:", (nltk.classify.accuracy(LinearSVC_classifier, testing_set))*100)

save_classifier = open("pickles/LinearSVC_classifier.pickle","wb")
pickle.dump(LinearSVC_classifier, save_classifier)
save_classifier.close()




#NuSVC_classifier = SklearnClassifier(NuSVC())
#NuSVC_classifier.train(training_set)
#print("NuSVC_classifier accuracy percent:", (nltk.classify.accuracy(NuSVC_classifier, testing_set))*100)
