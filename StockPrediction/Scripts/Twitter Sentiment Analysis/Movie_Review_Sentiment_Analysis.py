# -*- coding: utf-8 -*-
"""
Created on Sun Dec 8 22:10:20 2019

@author: hpins
"""

import SentimentModule as sentmod

print(sentmod.sentiment("This movie was awesome! The acting was great, plot was wonderful, and my kids loved it!"))
print(sentmod.sentiment("This movie was utter junk. There were no exciting scenes. I don't see what the point was at all. Horrible movie, 0/10"))
print(sentmod.sentiment("It, was okay"))
print(sentmod.sentiment("This movie was not the best"))
print(sentmod.sentiment("This movie was bad."))
print(sentmod.sentiment("wow, what an incredibly bad performance"))
