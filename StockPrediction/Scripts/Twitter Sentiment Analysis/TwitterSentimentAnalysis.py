# -*- coding: utf-8 -*-
"""
Created on Sat Dec 7 15:43:45 2019

@author: hpins
"""

import pandas as pd
import numpy as np
from sklearn import preprocessing
import matplotlib.pyplot as plt
from textblob import TextBlob
import re

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer

from tweepy import API
from tweepy import Cursor
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream

#%% ---------------------------------------------------------------------------
# ------------------------- Constants/Enums -----------------------------------
# -----------------------------------------------------------------------------

# file paths:
DATASET_PATH = "../../data/Sentiment/Tweets_Small.csv"

ACCESS_TOKEN = "1206324961671106562-3QtjkufzSjOxxg6m8rRuzCGnEKuguJ"
ACCESS_TOKEN_SECRET = "QOZeXheN0UAp6whaaCv4ZuctkNGA05tQQvL0qlorZ1oHr"
CONSUMER_KEY = "aAdoaObmeyPSEw8obHWGOAd3Y"
CONSUMER_SECRET = "n4o6hoRgSpJuEBl0exSIGBEA5J799lqv0vtRpCGtZeQQTP5hy5"


#%% ---------------------------------------------------------------------------
# -------------------------- Classes ------------------------------------------
# -----------------------------------------------------------------------------

class TwitterClient():
    def __init__(self, twitter_user=None):  # default none = our own twitter account
        self.auth = TwitterAuthenticator().authenticate_twitter_app()
        self.twitter_client = API(self.auth)
        self.twitter_user = twitter_user
        
    def get_twitter_client_api(self):
        return self.twitter_client
        
    def get_user_timeline_tweets(self, num_tweets):
        tweets = []
        
        # get tweets of our own account timeline
        for tweet in Cursor(self.twitter_client.user_timeline, id=self.twitter_user).items(num_tweets):
            tweets.append(tweet)
        
        return tweets
    
    def get_friend_list(self, num_friends):
        friend_list = []
        
        for friend in Cursor(self.twitter_client.friends).items(num_friends):
            friend_list.append(friend)
        
        return friend_list
    
    # getting top tweets of followed accounts
    def get_home_timeline_tweets(self, num_tweets):
        tweets = []
        
        for tweet in Cursor(self.twitter_client.home_timeline, id=self.twitter_user).items(num_tweets):
            tweets.append(tweet)
            
        return tweets

# Prints received tweets to stdout
class TwitterListener(StreamListener):
    
    def __init__(self, fetched_tweets_filename):
        self.fetched_tweets_filename = fetched_tweets_filename
    
    def on_data(self, data):
        try:
            #print(data)
            with open(self.fetched_tweets_filename, 'a') as tf:
                tf.write(data)
            return True
        except BaseException as e:
            print("Error on data: %s" % str(e))
        return True
    
    def on_error(self, status):
        # 420 --> rate limit error message by the twitter api
        if status == 420:
            return False
        print(status)
         
        
        
class TwitterAuthenticator():
    
    def authenticate_twitter_app(self):
        auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
        return auth
        
# Handles twitter authentication and the connection to the streaming api
class TwitterStreamer():
    
    def __init__(self):
        self.twitter_authenticator = TwitterAuthenticator()
    
    def stream_tweets(self, fetched_tweets_filename, hashtag_list):
        listener = TwitterListener(fetched_tweets_filename)
        
        auth = self.twitter_authenticator.authenticate_twitter_app()
        stream = Stream(auth, listener)
        
        # filter tweets:
        stream.filter(track=hashtag_list)
        

# Functionality for analyzing and categorizing contents of tweets
class TweetAnalyzer():
    
    def tweets_to_data_frame(self, tweets):
        df = pd.DataFrame(data=[tweet.text for tweet in tweets], columns=['tweets'])
        
        df['id'] = np.array([tweet.id for tweet in tweets])
        df['length'] = np.array([len(tweet.text) for tweet in tweets])
        df['date'] = np.array([tweet.created_at for tweet in tweets])
        df['source'] = np.array([tweet.source for tweet in tweets])
        df['retweets'] = np.array([tweet.retweet_count for tweet in tweets])
        
        likesList = []
        for tweet in tweets:
            # check if it was a retweet (needs a different approach for getting likes)
            if hasattr (tweet, 'retweeted_status'):
                likesList.append(tweet.retweeted_status.favorite_count)
            else:
                likesList.append(tweet.favorite_count)
        df['likes'] = np.array(likesList)
        
        return df
    
    # remove special characters, links, etc.
    def clean_tweet(self, tweet):
        return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", tweet).split())
        
    def analyze_sentiment(self, tweet):
        analysis = TextBlob(self.clean_tweet(tweet))
        
        # check if tweet is positive or negative
        if analysis.sentiment.polarity > 0:
            return 1
        elif analysis.sentiment.polarity == 0:
            return 0
        else:
            return -1

#%% ---------------------------------------------------------------------------
# -------------------------- Functions ----------------------------------------
# -----------------------------------------------------------------------------

def get_realtime_twitter_data():
    twitter_client = TwitterClient()
    tweet_analyzer = TweetAnalyzer()
    
    api = twitter_client.get_twitter_client_api()
    
    tweets = api.user_timeline(screen_name="realDonaldTrump", count=20)

    df = tweet_analyzer.tweets_to_data_frame(tweets)
    
    
    #print(df.head(3))
    #print(tweets[1].id)
    #print(dir(tweets[0]))
    #print(tweets[0].retweet_count)
    
    # get average length over all tweets
    print(np.mean(df['length']))
    
    # most liked tweet
    print(np.max(df['likes']))
    
    # most retweets
    print(np.max(df['retweets']))
    
    
    # time series
    # data will be on the y axis, index will be on the x axis
    time_likes = pd.Series(data=df['likes'].values, index=df['date'])
    time_likes.plot(figsize=(16, 4), label='likes', legend=True)
    
    time_retweets = pd.Series(data=df['retweets'].values, index=df['date'])
    time_retweets.plot(figsize=(16, 4), label='retweets', legend=True)
    plt.show()
    
    
    
    # sentiment analysis
    df['sentiment'] = np.array([tweet_analyzer.analyze_sentiment(tweet) for tweet in df['tweets']])
    
    print(df.head(10))
    
def process_offline_data_set():
    perform_preprocessing()
    
    textblob_sentiment_analysis()

    nltk_sentiment_analysis()
    
def perform_preprocessing():
    remove_stop_words()
    perform_stemming()
    
# removes stop words (aka fill words like 'with', etc.)    
def remove_stop_words():
    stop_words = set(stopwords.words("english"))
    index = 0
    for text in dataset['SentimentText']:    
        words = word_tokenize(text)
        filtered_text = []
        
        for word in words:
            if word not in stop_words:
                filtered_text.append(word)
                
        dataset.at[index, 'SentimentText'] = filtered_text
        
        index += 1
        
# identifies word stems to aggregate similar words
def perform_stemming():
    ps = PorterStemmer
    
    index = 0
    for text in dataset['SentimentText']:    
        words = word_tokenize(text)
        stemmed_text = []
        
        for word in words:
            stemmed_text.append(ps.stem(word))
                
        dataset.at[index, 'SentimentText'] = stemmed_text
        
        index += 1
    
    
def textblob_sentiment_analysis():
    ### Accuracy without preprocessing = 0.459 (small dataset) 
    
    # calc sentiments with textblob
    calculated_sentiments = []
    
    for tweet in dataset['SentimentText']:    
        text = tweet
        analysis = TextBlob(text)
        
        # check if tweet is positive or negative
        if analysis.sentiment.polarity > 0:
            calculated_sentiments.append(1)
        elif analysis.sentiment.polarity == 0:
            calculated_sentiments.append(0)
        else:
            calculated_sentiments.append(-1)
    
    dataset['CalculatedSentimentTextBlob'] = np.array(calculated_sentiments)
    
    
    # analyze textblob prediction accuracy
    correct_classification_count = 0
    
    for tweet in dataset.iterrows():
        if tweet[1]['CalculatedSentimentTextBlob'] == tweet[1]['Sentiment']:
            correct_classification_count += 1
            
    print("Textblob, correct abs: \t", correct_classification_count)
    
    textblob_score = correct_classification_count / len(dataset)

    print("Textblob, correct rel: \t", textblob_score)
    
def nltk_sentiment_analysis():
    # manually sadkf
    x = 3

#%% ---------------------------------------------------------------------------
# -------------------------- Code ---------------------------------------------
# -----------------------------------------------------------------------------

if __name__ == "__main__":
    # open nltk downloader and download all components:
    #nltk.download()    # (comment line when done)
    
    dataset = pd.read_csv(DATASET_PATH)
        
    #get_realtime_twitter_data()
    process_offline_data_set()
    