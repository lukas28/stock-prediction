# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 14:01:01 2019

@author: Dominik
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from enum import Enum
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import  accuracy_score
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier # multi-layer perceptron network
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
import warnings
import seaborn as sns
warnings.filterwarnings("ignore")


class Model(Enum):
    __order__ = 'NEARESTNEIGHBORS LINEARSVC RBFSVC DECISIONTREE RANDOMFOREST LOGISTICREGRESSION NEURALNETWORK'
    NEARESTNEIGHBORS = 1
    LINEARSVC = 2
    RBFSVC = 3
    DECISIONTREE = 4
    RANDOMFOREST = 5
    LOGISTICREGRESSION = 6
    NEURALNETWORK = 7

def load_data_dji():
    filename = "data/big/DJI.csv"
    df = pd.read_csv(filename)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_axis(df['Date'], inplace=True)
    df.drop(columns=['High', 'Low', 'Volume'], inplace=True)
    open_data = df['Open']
    close_data = df['Close']

    return open_data, close_data

def load_data_nikkei():
    filename = "data/big/nikkei.csv"
    df = pd.read_csv(filename)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_axis(df['Date'], inplace=True)
    df.drop(columns=['High', 'Low', 'Volume'], inplace=True)
    open_data = df['Open']
    close_data = df['Close']

    return open_data, close_data

def load_data_oil():
    filename = "data/big/oil_cut.csv"
    df = pd.read_csv(filename)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_axis(df['Date'], inplace=True)
    price = df['Price']

    return price

def load_data_euro():
    filename = "data/big/exchange_rate_EUR.csv"
    df = pd.read_csv(filename)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_axis(df['Date'], inplace=True)
    rate = df['Exchange rate']

    return rate

def load_data_china():
    filename = "data/big/exchange_rate_CHI.csv"
    df = pd.read_csv(filename)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_axis(df['Date'], inplace=True)
    rate = df['Exchange rate']

    return rate

def load_data_gbp():
    filename = "data/big/exchange_rate_GBP.csv"
    df = pd.read_csv(filename)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_axis(df['Date'], inplace=True)
    rate = df['Exchange rate']

    return rate

#Make time series with window size
def make_time_series(data,win):
    as_strided = np.lib.stride_tricks.as_strided  
    return as_strided(data, (len(data) - (win - 1), win), (data.values.strides * 2))

#Dvide current day / previous day
def roll_diff(data):
    difference=[]
    difference.append(np.nan)
    for i in (range(data.shape[0])[1:-1]):
        difference.append(data[i]/data[i-1])
    difference=pd.Series(difference)
    return difference

#Plot features
def plot_difference(data,title,name):
    fig = plt.figure(figsize=(25, 6))
    fig.suptitle(title+" time series", fontsize=16)
    plt.plot(data)
    plt.ylabel('difference')
    plt.xlabel('day')
    plt.savefig(name+".svg")
   
#Calculate correlation
def correlate(data1,data2):
    temp1=data1.iloc[1:-1]
    temp2=data2.dropna()
    temp1.reset_index(drop=True, inplace=True)
    print(temp1.corr(temp2))
    
#Get raw data of the datasets
open_data_raw, close_data_raw = load_data_dji()
n_open_data_raw, n_close_data_raw = load_data_nikkei()
oil_data_raw=load_data_oil()
euro_data_raw=load_data_euro()
china_data_raw=load_data_china()
gbp_data_raw=load_data_gbp()

#Add them together and remove days which don't have values for all features
concat_data=pd.concat([open_data_raw,close_data_raw,oil_data_raw,euro_data_raw,china_data_raw,gbp_data_raw,n_open_data_raw,n_close_data_raw],axis=1, ignore_index=True).dropna()

#Seperate them again to be able to make different extractions
open_data=concat_data[0]
close_data=concat_data[1]
oil_data=concat_data[2]
euro_data=concat_data[3]
china_data=concat_data[4]
gbp_data=concat_data[5]
n_open_data=concat_data[6]
n_close_data=concat_data[7]

#Get DJI day difference
difference=close_data/open_data
signed_difference=np.sign(close_data-open_data).astype(int)

#Get NIKKEI day difference
n_difference=n_close_data/n_open_data
n_signed_difference=np.sign(n_close_data-n_open_data).astype(int)

#Get day differences of oil,usd/euro,usd/yuan,usd/gbp
oil_difference=roll_diff(oil_data)
euro_data=roll_diff(euro_data)
china_data=roll_diff(china_data)
gbp_data=roll_diff(gbp_data)

#Windows size we want to look back
window_size=5

signed_difference=signed_difference.iloc[window_size:]
signed_difference.reset_index(drop=True, inplace=True)

#Manioulate indexes to fit them ti window size
n_difference.reset_index(drop=True, inplace=True)
open_df=open_data.iloc[window_size:]
open_df.reset_index(drop=True, inplace=True)
closed_df=close_data.iloc[window_size:]
closed_df.reset_index(drop=True, inplace=True)
n_open_df=n_open_data.iloc[window_size:]
n_open_df.reset_index(drop=True, inplace=True)
n_closed_df=n_close_data.iloc[window_size:]
n_closed_df.reset_index(drop=True, inplace=True)

# PLot the features
plot_difference(difference,"DJI day difference","dji_difference")
plot_difference(oil_difference,"Oil day difference","oil_difference")
plot_difference(euro_data,"USD/EURO day difference","euro_difference")
plot_difference(china_data,"USD/YUAN day difference","yuan_difference")
plot_difference(gbp_data,"USD/GBP day difference","gbp_difference")

# Get correlations of the feautres to the dji difference
correlate(difference,oil_difference)
correlate(difference,euro_data)
correlate(difference,china_data)
correlate(difference,gbp_data)

# Make 5 days series per predicition day
data_series_open=pd.DataFrame(make_time_series(open_data,window_size+1))
data_series_n_difference=pd.DataFrame(make_time_series(n_difference,window_size))
data_series_closed=pd.DataFrame(make_time_series(close_data,window_size))
data_series_difference=pd.DataFrame(make_time_series(difference,window_size))
data_series_oil=pd.DataFrame(make_time_series(oil_difference,window_size))
data_series_euro=pd.DataFrame(make_time_series(euro_data,window_size))
data_series_china=pd.DataFrame(make_time_series(china_data,window_size))
data_series_gbp=pd.DataFrame(make_time_series(gbp_data,window_size))

#DJI only dataset
#full_dataset=pd.concat([data_series_open,data_series_closed.iloc[0:-1,:],pd.DataFrame(signed_difference),open_df,closed_df], axis=1, ignore_index=True)
#DJI oil dataset
#full_dataset=pd.concat([data_series_difference.iloc[0:-1,:],data_series_oil.iloc[0:-1,:],pd.DataFrame(signed_difference),open_df,closed_df], axis=1, ignore_index=True)
#DJI oil euro yuan pound dataset
full_dataset=pd.concat([data_series_difference.iloc[0:-1,:],data_series_oil.iloc[0:-1,:],data_series_euro.iloc[0:-1,:],data_series_china.iloc[0:-1,:],data_series_gbp.iloc[0:-1,:],pd.DataFrame(signed_difference),open_df,closed_df], axis=1, ignore_index=True)
#DJI oil NIKKEI dataset
#full_dataset=pd.concat([data_series_difference.iloc[0:-1,:],data_series_oil.iloc[0:-1,:],data_series_n_difference.iloc[:,:],pd.DataFrame(signed_difference),open_df,closed_df], axis=1, ignore_index=True)

#Remove first and last day because if windows
full_dataset=full_dataset.dropna()

# Taking the first 100 pixels and checking the correlation
plt.figure(figsize=(20,20))
heatmap=sns.heatmap(full_dataset.iloc[:,0:26].corr(),vmin=-1,cmap='coolwarm');
heatmap.get_figure().savefig("correlation_heatmap.svg")

#Continous heldback dataset
heldback=full_dataset.iloc[-128:,:]
dataset=full_dataset.iloc[:-128,:]

# Encode the string-labels to integers - needed for some models
le = preprocessing.LabelEncoder()
le.fit(pd.unique(dataset.iloc[:,-1]))

#2nd heldback dataset
data_train, data_test, target_train, target_test = train_test_split(dataset.iloc[:,0:-3], dataset.iloc[:,-3:], train_size=0.80,random_state=7986, stratify=dataset.iloc[:,-3])
evaluate_train=target_train
target_train=target_train.iloc[:,0]
        
# Define the classifiers behind the model models
classifiers = {
    Model.NEARESTNEIGHBORS:     KNeighborsClassifier(),
    Model.LINEARSVC:            SVC(kernel='linear', probability = True),
    Model.RBFSVC:               SVC(kernel='rbf'),
    Model.DECISIONTREE:         DecisionTreeClassifier(),
    Model.RANDOMFOREST:         RandomForestClassifier(),
    Model.LOGISTICREGRESSION:   LogisticRegression(solver='lbfgs'),
    Model.NEURALNETWORK:        MLPClassifier(solver='adam', alpha=1e-5, random_state=1)
}
        
# Define the tunable hyper parameters for the models
params = {
    Model.NEARESTNEIGHBORS:     {'n_neighbors': [n for n in range(1,16)]},
    Model.RBFSVC:               {'C': [3**n for n in range(-3,4)], 'gamma':[3**n for n in range(-3,4)]},
    Model.DECISIONTREE:         {'max_depth': [n for n in range(1,11)]},
    Model.RANDOMFOREST:         {'max_depth': [n for n in range(1,11)], 'n_estimators':[n for n in range(1,11)]},
    Model.LOGISTICREGRESSION:   {"C": np.logspace(-3,3,7), "penalty":["l2"]},
    Model.NEURALNETWORK:        {'hidden_layer_sizes': [(5,10,5,1),(10,9,7,1),(25,29,30,1)], 'max_iter': [100,150,200,300,500] }
}
        
# Empty Array to insert models afterwards
grid_searches = {}
        
modelsToIgnore = [Model.NEARESTNEIGHBORS,Model.DECISIONTREE,Model.RANDOMFOREST,Model.LOGISTICREGRESSION,Model.NEURALNETWORK,Model.LINEARSVC]
modelsToIgnore=[]
modelsToIgnore = [Model.LINEARSVC]

#Own scoring function to get improvement to normal index movement
def trading_scoring_func_est(estimator, X, y):
    temp=evaluate_train.loc[y.index]
    normalmoney=1000*((temp.iloc[:,-1]/temp.iloc[:,-2]).prod())
    temp=temp[estimator.predict(X)==1]
    predictedmoney=1000*((temp.iloc[:,-1]/temp.iloc[:,-2]).prod())
    return predictedmoney/normalmoney

# Perform a grid search for all defined models and parameter configurations
for model in Model:
        
    # Conditional used for testing: Deactivates all models within the array
    if (model in modelsToIgnore):
        continue
        
    print("Running GridSearchCV for %s." % model)
        
    # Create pipeline with scaling, PCA and classifiers
    pipe = Pipeline([
        # Scale so that mean is 0 and std is 1
        ('normalization', StandardScaler()),
        
        # Set classifier to the corresponding model
        ('classify', classifiers[model])
    ])
        
    # Hyperparameter range
    parameters_to_tune = params[model]
        
    # Translate paramters so that the pipeline can use it
    param_grid = {}
    for key,value in parameters_to_tune.items():
        hyperparam_key = "classify__" + key
        param_grid[hyperparam_key] = value
        
        # Make Grid Search with cv 10
        gs = GridSearchCV(pipe, param_grid=param_grid, cv=10, scoring=trading_scoring_func_est)
        
        # Train the models
        gs.fit(data_train, target_train)
        
        # Store the models in the array
        grid_searches[model] = gs
        
        
        
# ----------------------------------
# Evaluate model performance
# ----------------------------------
print("\n\n########### Model Performance Evaluation ##########\n")
for model in Model:
    # Conditional used for testing: Deactivates all models within the array
    if (model in modelsToIgnore):
        continue
        
    best_estimator = grid_searches[model].best_estimator_
        
    # Predict faces from heldback test dataset with best estimator. This also does the scaling and PCA of the best estimator
    predicted_gestures = best_estimator.predict(data_test)
                    
    # Calculate accuracy and print it
    print("---------------------------------")
    print("Model: ", model)
    print("Best score: " + str(grid_searches[model].best_score_))
    print("Best params: " + str(grid_searches[model].best_params_))
    
    print("---------------------------------\n")
print("###################################################")

#Extract scores and plot them in a boxplot
best_scores=[]
for model in Model:
    best_scores.append([])
    
for model in Model:
    if (model in modelsToIgnore):
        continue
    for splitIndex in range(10):
        best_scores[model.value-1].append(grid_searches[model].cv_results_["split"+str(splitIndex)+"_test_score"][grid_searches[model].best_index_])
del best_scores[1:3]

fig = plt.figure(figsize=(15, 6))
fig.suptitle('Boxplot of best model scores of grid search using dji only', fontsize=16)
plt.boxplot(best_scores)
plt.ylabel('Score')
plt.xlabel('Model Type')
plt.xticks(np.arange(6), ('', 'Nearest Neighbour','RBFSVC', 'Decision Tree', 'Random Forest', 'Logistic Regression', 'Neural Network'))
fig.savefig("dji_only_best_scores_scores.svg")

#Get reference values for heldback test sets = normal market movement
money=1000*((target_test.iloc[:,-1]/target_test.iloc[:,-2]).prod())
print("Normal money heldback asdf: "+str(money))

held_data=heldback.iloc[:,:-3]
held_target=heldback.iloc[:,-3:]
money=1000*((held_target.iloc[:,-1]/held_target.iloc[:,-2]).prod())
print("Normal money heldback month asdf: "+str(money))

#Get sdcore of best model on heldback test set
model=grid_searches[Model.RBFSVC].best_estimator_

temp=target_test[model.predict(data_test)==1]
money=1000*((temp.iloc[:,-1]/temp.iloc[:,-2]).prod())
print("Remaining money heldback: "+str(money))

held_data=heldback.iloc[:,:-3]
held_target=heldback.iloc[:,-3:]
temp=held_target[model.predict(held_data)==1]
money=1000*((temp.iloc[:,-1]/temp.iloc[:,-2]).prod())
print("Remaining money heldback month: "+str(money))

