# -*- coding: utf-8 -*-
"""
Created on Sat Dec 7 20:58:36 2019

@author: hpins
"""

import nltk
import random
from nltk.tokenize import word_tokenize
from nltk.classify.scikitlearn import SklearnClassifier
import pickle

from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC

from nltk.classify import ClassifierI
from statistics import mode





class VoteClassifier(ClassifierI):
    def __init__(self, *classifiers):
        self._classifiers = classifiers

    def classify(self, features):
        votes = []
        
        for classifier in self._classifiers:
            vote = classifier.classify(features)
            votes.append(vote)
        return mode(votes)

    def confidence(self, features):
        votes = []
        
        for classifier in self._classifiers:
            vote = classifier.classify(features)
            votes.append(vote)

        chosen_votes_count = votes.count(mode(votes))
        confidence_level = chosen_votes_count / len(votes)
        return confidence_level
        


documents_f = open("pickles/documents.pickle", "rb")
documents = pickle.load(documents_f)
documents_f.close()

word_features_f = open("pickles/word_features.pickle", "rb")
word_features = pickle.load(word_features_f)
word_features_f.close()
    



def find_features(document):
    words = word_tokenize(document)
    features = {}
    for w in word_features:
        features[w] = (w in words)

    return features



featuresets_f = open("pickles/featuresets.pickle", "rb")
featuresets = pickle.load(featuresets_f)
featuresets_f.close()


# shuffle in order to place feature sets randomly
random.shuffle(featuresets)
print("Featuresets length: ", len(featuresets))


# create training and test split:      
training_set = featuresets[:10000]
testing_set =  featuresets[10000:]




open_file = open("pickles/NaiveBayes_classifier.pickle", "rb")
NaiveBayes_classifier = pickle.load(open_file)
open_file.close()


open_file = open("pickles/MNB_classifier.pickle", "rb")
MNB_classifier = pickle.load(open_file)
open_file.close()



open_file = open("pickles/BernoulliNB_classifier.pickle", "rb")
BernoulliNB_classifier = pickle.load(open_file)
open_file.close()


open_file = open("pickles/LogisticRegression_classifier.pickle", "rb")
LogisticRegression_classifier = pickle.load(open_file)
open_file.close()


open_file = open("pickles/LinearSVC_classifier.pickle", "rb")
LinearSVC_classifier = pickle.load(open_file)
open_file.close()


open_file = open("pickles/SGDC_classifier.pickle", "rb")
SGDC_classifier = pickle.load(open_file)
open_file.close()



voted_classifier = VoteClassifier(NaiveBayes_classifier,
                                  LinearSVC_classifier,
                                  MNB_classifier,
                                  BernoulliNB_classifier,
                                  LogisticRegression_classifier)


def sentiment(text):
    feats = find_features(text)
    return voted_classifier.classify(feats),voted_classifier.confidence(feats)