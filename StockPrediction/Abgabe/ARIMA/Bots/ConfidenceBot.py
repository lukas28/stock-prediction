# This kind of bot uses the base functionality of the SimpleBot implementation.
# Other than the SimpleBot, the ConfidenceBot needs additionally to the trend (Buy/sell) the
# prediction percentage.
# If the model predicts a rise with a confidence higher than the confidence sell threshold, the bot buys
# If the model predicts a fall and the confidence is higher than the threshold, the model sells

from SimpleBot import SimpleBot

class ThresholdBot(SimpleBot):

	def __init__(buyConfidenceThreshold, sellConfidenceThreshold):
		super().__init__()
		self.lastIndex = 0
		self.buyConfidenceThreshold = buyThreshold
		self.sellConfidenceThreshold = sellThreshold

	def trade(currentIndex, modelInput, modelConfidence)
		if self.currentlyIn == False and modelConfidence > buyThreshold:
			self.buy(currentIndex)
		if self.currentlyIn == True and modelConfidence < sellThreshold:
			self.sell(currentIndex)
