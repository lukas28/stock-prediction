import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import lag_plot
from pandas import datetime
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
from Bots.SimpleBot import SimpleBot
from statsmodels.tsa.stattools import adfuller
from numpy import log
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from pmdarima.arima.utils import ndiffs
from statsmodels.tsa.arima_model import ARIMA

from statsmodels.tsa.arima_model import ARIMA
import pmdarima as pm
import numpy as np


# create a simple bot instance which takes care of trading based on the
# models forecast
bot = SimpleBot()

# load dataset and restrict data to the last 2500 days
df = pd.read_csv("data/DJI.csv").fillna(0)
df = df[-2500:]
df = df.reset_index()
df.head()

# calculate reference return of invest for comparing own results
index_roi = 100 * df.Open.iloc[-1] / df.Open.iloc[0]

# this function takes data which is used to fit the model on and makes the
# forecast. If the model predicts rise it returns 1 and 0 otherwise
def makeForecast(train_data, test_data):
    train_data, test_data = df[0:int(len(df)*0.7)], df[int(len(df)*0.7):]
    test_data = test_data.reset_index()

    model_fit = pm.auto_arima(train_data['Open'], start_p=1, start_q=1,
                          test='adf',       # use adftest to find optimal 'd'
                          max_p=10, max_q=10, # maximum p and q
                          m=1,              # frequency of series
                          d=None,           # let model determine 'd'
                          seasonal=False,   # No Seasonality
                          start_P=0,
                          D=0,
                          trace=False,
                          error_action='ignore',
                          suppress_warnings=True,
                          stepwise=True)

    # Actual vs Fitted
    output, confint = model_fit.predict(n_periods=len(test_data['Open']), return_conf_int=True)

    if output[-1] > output[0]:
        return 1
    else:
        return 0



# set train dataset size and forecasting lag appropriately
train_lag = 20
predict_lag = 3


# Iterate over the data and make predictions/trades
while len(df.Open) > (train_lag + predict_lag):
    train_data = df.Open.iloc[:train_lag]
    test_data = df.Open.iloc[train_lag:(train_lag+predict_lag)]
    test_data.reset_index()
    should_trade = makeForecast(train_data, test_data)
    bot.trade(train_data.iloc[-1],should_trade)

    df.drop(df.index[0], inplace = True)
    #df.drop(df.index[0:predict_lag], inplace = True)

# print out the model roi and the index roi as reference
print('traded roi: ' + str(bot.getRoi()))
print('index roi: '+ str(index_roi))
