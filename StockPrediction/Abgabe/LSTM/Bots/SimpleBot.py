# This simple bot gets the information from the model whether to buy or sell stocks.
# If the model suggests buying and the bot does not hold stocks --> buy with all the money
# If the model suggests selling and the bot does hold stocks --> sell all the stocks
# Depending on the success of the strategy, the ROI will rise or fall
# Output of the bot is the ROI (Return of invest) in %


class SimpleBot():

	def __init__(self, verbose = False):
		self.startCapital = 1000
		self.capital = self.startCapital
		self.currentlyIn = False
		self.boughtIndex = 0
		self.verbose = verbose
		self.positiveTrades = 0
		self.negativeTrades = 0

	def trade(self,currentIndex, modelInput):
		if modelInput == 1 and self.currentlyIn == False:
			self.buy(currentIndex)
			return 1
		if modelInput == 0 and self.currentlyIn == True:
			self.sell(currentIndex)
			return 0
		return -1

	def buy(self, currentIndex):
		if not self.verbose: print('bought stocks for: ' + str(currentIndex))
		self.boughtIndex = currentIndex
		self.currentlyIn = True

	def sell(self,currentIndex):
		if not self.verbose: print('sold stocks for: ' + str(currentIndex))
		self.currentlyIn = False
		tradeFactor = (currentIndex * 100 / self.boughtIndex) / 100
		self.capital = self.capital * tradeFactor

		if tradeFactor > 1:
			self.positiveTrades += 1
		else:
			self.negativeTrades += 1
		#print(self.capital)

	def getRoi(self, index):
		if self.currentlyIn:
			self.sell(index)
		print('cash start: ' + str(self.startCapital) + 'cash end: ' + str(self.capital))
		print('pos: ' + str(self.positiveTrades))
		print('neg: ' + str(self.negativeTrades))
		return (self.capital * 100 / self.startCapital)
