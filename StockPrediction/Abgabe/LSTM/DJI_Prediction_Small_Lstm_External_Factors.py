# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 16:45:07 2019

@author: Lukas
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 14:01:01 2019

@author: Lukas
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pywt
import seaborn as sns
from enum import Enum
from sklearn import preprocessing, decomposition
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier # multi-layer perceptron network
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
import warnings
from sklearn.decomposition import PCA
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.model_selection import  train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import math
warnings.filterwarnings("ignore")


# define methods to load datasets
def load_data_dji():
    filename = "../data/DJI.csv"
    df = pd.read_csv(filename)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_axis(df['Date'], inplace=True)
    df.drop(columns=['High', 'Low', 'Volume'], inplace=True)
    open_data = df['Open']
    close_data = df['Close']

    return open_data, close_data

def load_data_nikkei():
    filename = "../data/nikkei.csv"
    df = pd.read_csv(filename)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_axis(df['Date'], inplace=True)
    df.drop(columns=['High', 'Low', 'Volume'], inplace=True)
    open_data = df['Open']
    close_data = df['Close']

    return open_data, close_data

def load_data_oil():
    filename = "../data/oil_cut.csv"
    df = pd.read_csv(filename)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_axis(df['Date'], inplace=True)
    price = df['Price']

    return price


# load the datasets
open_data_raw, close_data_raw = load_data_dji()
n_open_data_raw, n_close_data_raw = load_data_nikkei()
oil_data_raw=load_data_oil()

series = 7

# function that creates the dataset suitable as lstm input
def create_lstm_dataset(df, series):
    print(df.shape[0])
    counter = series
    data = df.iloc[0]
    data = np.hstack((df.iloc[0],df.iloc[1],df.iloc[2],df.iloc[3],df.iloc[4],df.iloc[5],df.iloc[6]))
    result = df.iloc[1][0]
    while (counter+series) < df.shape[0]-1:
        tmp = df.iloc[counter]
        for i in range(series-1):
            tmp = np.hstack([tmp, df.iloc[counter+i+1]])
            if i+1 == series-1:
                data = np.vstack([data,tmp])
        result = np.vstack([result, df.iloc[counter+series+1][0]])
        counter+=1
    return data, result



# merge datasets and create lstm siutable dataset
concat_data=pd.concat([open_data_raw,oil_data_raw,n_open_data_raw],axis=1, ignore_index=True).dropna()
concat_data = concat_data.iloc[:5000] #use only first 5000 for training

scaler = MinMaxScaler(feature_range=(0,1))
print(concat_data.shape)
concat_data = scaler.fit_transform(concat_data)
print(concat_data.shape)
concat_data = pd.DataFrame(concat_data)

d, r = create_lstm_dataset(concat_data, series)


trainX = np.reshape(d, (d.shape[0], 7, 3))


#build the model
model = Sequential()
model.add(LSTM(4, input_shape=(series, 3)))
model.add(Dense(1))
model.compile(loss='mse', optimizer='adam')
#fit the model and save it
model.fit(trainX, r, epochs=200, batch_size=64)
model.save('lstm_small_extern_ep200_bs32.h5')
