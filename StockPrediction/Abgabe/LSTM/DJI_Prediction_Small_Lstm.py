#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 08:20:26 2019

@author: lukashubl
"""

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.model_selection import  train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import math

np.random.seed(7)

#load the dataset
df = pd.read_csv('../data/DJI.csv')
df.head()
df['Date'] = pd.to_datetime(df['Date'])
df['Close'] = pd.to_numeric(df['Close'], downcast='float')
df.set_index('Date',inplace=True)
df.info()
df.sort_index(inplace=True)

#extract just close prices as that is what we want to predict
df_close = df['Close']
df_close = df_close.values.reshape(len(df_close), 1)
plt.plot(df_close)
plt.show()

#normalize data
scaler = MinMaxScaler(feature_range=(0,1))
df_close = scaler.fit_transform(df_close)
df_close

# create train and test set
msft_train = df_close[0:5000, :]
msft_test = df_close[5000:len(df_close), :]

print('Split data into train and test: ', len(msft_train), len(msft_test))

#need to now convert the data into time series looking back over a period of days...e.g. use last 7 days to predict price
def create_ts(ds, series):
    X, Y =[], []
    for i in range(len(ds)-series - 1):
        item = ds[i:(i+series), 0]
        X.append(item)
        Y.append(ds[i+series, 0])
    return np.array(X), np.array(Y)

series = 7

# create lstm suitable datasets
trainX, trainY = create_ts(msft_train, series)
testX, testY = create_ts(msft_test, series)

#reshape into  LSTM format - samples, steps, features
trainX = np.reshape(trainX, (trainX.shape[0], trainX.shape[1], 1))
testX = np.reshape(testX, (testX.shape[0], testX.shape[1], 1))

#build the model
model = Sequential()
model.add(LSTM(4, input_shape=(series, 1)))
model.add(Dense(1))
model.compile(loss='mse', optimizer='adam')
#fit the model
model.fit(trainX, trainY, epochs=100, batch_size=32)


#test this model out
trainPredictions = model.predict(trainX)
testPredictions = model.predict(testX)
#unscale predictions
trainPredictions = scaler.inverse_transform(trainPredictions)
testPredictions = scaler.inverse_transform(testPredictions)
trainY = scaler.inverse_transform([trainY])
testY = scaler.inverse_transform([testY])

#lets calculate the root mean squared error
trainScore = math.sqrt(mean_squared_error(trainY[0], trainPredictions[:, 0]))
testScore = math.sqrt(mean_squared_error(testY[0], testPredictions[:, 0]))
print('Train score: %.2f rmse', trainScore)
print('Test score: %.2f rmse', testScore)

#lets plot the predictions on a graph and see how well it did
train_plot = np.empty_like(df_close)
train_plot[:,:] = np.nan
train_plot[series:len(trainPredictions)+series, :] = trainPredictions

test_plot = np.empty_like(df_close)
test_plot[:,:] = np.nan
test_plot[len(trainPredictions)+(series*2)+1:len(df_close)-1, :] = testPredictions

#plot on graph
plt.plot(scaler.inverse_transform(df_close))
plt.plot(train_plot)
plt.plot(test_plot)
plt.show()

# save the model
model.save('lstm_small_ep_100_bs32.h5')
