"""
Created on Wed Dec  16 08:20:26 2019

@author: lukashubl
"""

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.model_selection import  train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import math
import keras
from Bots.SimpleBot import SimpleBot

model = keras.models.load_model('Models/lstm_big_extern_ep200_bs32.h5')

# method to create lstm suitable dataset
def create_lstm_dataset(df, series):
    print(df.shape[0])
    counter = series
    data = df.iloc[0]
    data = np.hstack((df.iloc[0],df.iloc[1],df.iloc[2],df.iloc[3],df.iloc[4],df.iloc[5],df.iloc[6]))
    result = df.iloc[1][0]
    while (counter+series) < df.shape[0]-1:
        tmp = df.iloc[counter]
        for i in range(series-1):
            tmp = np.hstack([tmp, df.iloc[counter+i+1]])
            if i+1 == series-1:
                data = np.vstack([data,tmp])
        result = np.vstack([result, df.iloc[counter+series+1][0]])
        counter+=1
    return data, result

# method to merge the djia with external datasets
def create_multi_dataset():
    def load_data_dji():
        filename = "../data/DJI.csv"
        df = pd.read_csv(filename)

        df['Date'] = pd.to_datetime(df['Date'])
        df.set_axis(df['Date'], inplace=True)
        df.drop(columns=['High', 'Low', 'Volume'], inplace=True)
        open_data = df['Open']
        close_data = df['Close']

        return open_data, close_data

    def load_data_nikkei():
        filename = "../data/nikkei.csv"
        df = pd.read_csv(filename)

        df['Date'] = pd.to_datetime(df['Date'])
        df.set_axis(df['Date'], inplace=True)
        df.drop(columns=['High', 'Low', 'Volume'], inplace=True)
        open_data = df['Open']
        close_data = df['Close']

        return open_data, close_data

    def load_data_oil():
        filename = "../data/oil_cut.csv"
        df = pd.read_csv(filename)

        df['Date'] = pd.to_datetime(df['Date'])
        df.set_axis(df['Date'], inplace=True)
        price = df['Price']

        return price

    open_data_raw, close_data_raw = load_data_dji()
    n_open_data_raw, n_close_data_raw = load_data_nikkei()
    oil_data_raw=load_data_oil()


    concat_data=pd.concat([open_data_raw,oil_data_raw,n_open_data_raw],axis=1, ignore_index=True).dropna()
    print(concat_data.head())
    concat_data = concat_data.iloc[5000:]


    #scale dji
    dji_series = concat_data[0]
    dji_scaler = MinMaxScaler(feature_range=(0,1))
    dji_series_scaled = dji_scaler.fit_transform(pd.DataFrame(dji_series))
    concat_data[0] = dji_series_scaled

    #scale oil
    oil_series = concat_data[1]
    oil_scaler = MinMaxScaler(feature_range=(0,1))
    oil_series_scaled = oil_scaler.fit_transform(pd.DataFrame(oil_series))
    concat_data[1] = oil_series_scaled

    #scale nikkei
    nikkei_series = concat_data[2]
    nikkei_scaler = MinMaxScaler(feature_range=(0,1))
    nikkei_series_scaled = nikkei_scaler.fit_transform(pd.DataFrame(nikkei_series))
    concat_data[2] = nikkei_series_scaled

    concat_data = pd.DataFrame(concat_data)
    d, r = create_lstm_dataset(concat_data, series)

    return d, r, dji_scaler


series = 7

# create dataset
input, output, scaler = create_multi_dataset()
test = output.tolist()
test = scaler.inverse_transform(test)

#create trading bot
bot = SimpleBot()

# do the predictions and trading
predictions = []
for i in range(input.shape[0]):
    pred = scaler.inverse_transform(model.predict(np.reshape(input[i], (1,7,3))))
    predictions.append(pred[0][0])
    if scaler.inverse_transform(np.reshape(input[i][-3], (1,1))) < pred:
        bot.trade(scaler.inverse_transform(np.reshape(input[i][-3], (1,1))), 1)
    elif scaler.inverse_transform(np.reshape(input[i][-3], (1,1))) > pred:
        bot.trade(scaler.inverse_transform(np.reshape(input[i][-3], (1,1))), 0)

# show the results
print('index roi: ' + str(test[-1]*100/test[0]))
print('traded roi: ' + str(bot.getRoi(scaler.inverse_transform(np.reshape(input[-1][-3], (1,1))))))
plt.plot(predictions, label = "predicted")
plt.plot(test, label = "real")
plt.legend()
plt.show()
