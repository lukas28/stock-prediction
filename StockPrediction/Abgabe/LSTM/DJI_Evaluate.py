"""
Created on Wed Dec  16 08:20:26 2019

@author: lukashubl
"""

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.model_selection import  train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import math
import keras
from Bots.SimpleBot import SimpleBot

model = keras.models.load_model('Models/lstm_small_ep_200_bs32.h5')


#load the dataset
djia_dataset = pd.read_csv('../data/DJI.csv')
djia_dataset.head()
djia_dataset['Date'] = pd.to_datetime(djia_dataset['Date'])
djia_dataset['Close'] = pd.to_numeric(djia_dataset['Close'], downcast='float')
djia_dataset.set_index('Date',inplace=True)
djia_dataset.info()
djia_dataset.sort_index(inplace=True)

#extract just close prices as that is what we want to predict
djia_close = djia_dataset['Close']
djia_close = djia_close.values.reshape(len(djia_close), 1)

#normalize data
scaler = MinMaxScaler(feature_range=(0,1))

#train test split
djia_train = djia_close[0:5000, :]
djia_test = djia_close[5000:len(djia_close), :]
indexRoi = djia_test[-1]*100/djia_test[0]

print(djia_test[0])
print(djia_test[-1])
print('index roi: ' + str(djia_test[-1]*100/djia_test[0]))

djia_test = scaler.fit_transform(djia_test)

# method to create dataset
def create_ts(ds, series):
    X, Y =[], []
    for i in range(len(ds)-series - 1):
        item = ds[i:(i+series), 0]
        X.append(item)
        Y.append(ds[i+series, 0])
    return np.array(X), np.array(Y)

series = 7


# create dataset
input, output = create_ts(djia_test, series)
input = np.reshape(input, (input.shape[0], input.shape[1], 1))

#create trading bot
bot = SimpleBot(verbose = False)

predictions = []
for i in range(input.shape[0]):
    pred = scaler.inverse_transform(model.predict(np.reshape(input[i], (1,7,1))))
    predictions.append(pred[0][0])

    if scaler.inverse_transform(np.reshape(input[i][-1], (1,1))) < pred:
        bot.trade(scaler.inverse_transform(np.reshape(input[i][-1], (1,1))), 1)
    elif scaler.inverse_transform(np.reshape(input[i][-1], (1,1))) > pred:
        bot.trade(scaler.inverse_transform(np.reshape(input[i][-1], (1,1))), 0)


# show results
print('traded roi: ' + str(bot.getRoi(scaler.inverse_transform(np.reshape(input[-1][-1], (1,1))))))
print('index roi: ' + str(indexRoi))
plt.plot(predictions, label = "predicted")
plt.show()
