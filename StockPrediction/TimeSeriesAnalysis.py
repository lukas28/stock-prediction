from dateutil.parser import parse
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

dataset_name = 'nikkei'

plt.rcParams.update({'figure.figsize': (10, 7), 'figure.dpi': 120})

df = pd.read_csv('data/big/nikkei.csv', parse_dates=['Date'], index_col = 'Date')
df = df.dropna()


###################################
### VISUALIZING THE TIME SERIES ###
###################################
# Draw Plot
def plot_df(df, x, y, title="", xlabel='Date', ylabel='Value', dpi=100):
    fig = plt.figure(figsize=(16,5), dpi=dpi)
    plt.plot(x, y, color='tab:red')
    plt.gca().set(title=title, xlabel=xlabel, ylabel=ylabel)
    #plt.show()
    fig.savefig('Plots/Nikkei/' + dataset_name + '_series.png')


def plot_df_flipped_filled(df):
    fig, ax = plt.subplots(1, 1, figsize=(16,5), dpi= 120)
    plt.fill_between(df.index, y1=df.Open, y2=-df.Open, alpha=0.5, linewidth=2, color='seagreen')
    plt.ylim(-40000, 40000)
    plt.title('Daily Nikkei Open 1990-2019', fontsize=16)
    plt.hlines(y=0, xmin=np.min(df.index), xmax=np.max(df.index), linewidth=.5)
    #plt.show()
    fig.savefig('Plots/Nikkei/' + dataset_name + "_flipped_filled.png")

def plot_boxplots_yearwise(df):
    # Prepare data
    df['year'] = [d.year for d in df.index]
    df['month'] = [d.strftime('%b') for d in df.index]
    years = df['year'].unique()

    # Draw Plot
    #fig, axes = plt.subplots(2, 1, figsize=(12,10), dpi= 80)
    fig = plt.figure(figsize=(16,7), dpi = 120)
    ax = sns.boxplot(x='year', y='Open', data=df)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=40, ha="right")
    #plt.tight_layout()
    plt.title('Year-wise Box Plot\n(The Trend)', fontsize=16)

    fig2 = plt.figure(figsize=(16,7), dpi = 120)
    sns.boxplot(x='month', y='Open', data=df.loc[~df.year.isin([1985, 2019]), :])
    fig2.suptitle('Month-wise Box Plot\n(The Seasonality)', fontsize=16)

    # Set Title
    #axes[0].set_title('Year-wise Box Plot\n(The Trend)', fontsize=18);
    #axes[1].set_title('Month-wise Box Plot\n(The Seasonality)', fontsize=18)
    #plt.show()
    fig.savefig('Plots/Nikkei/' + dataset_name + '_boxplots_year.png')
    fig2.savefig('Plots/Nikkei/' + dataset_name + '_boxplots_month.png')


#plot_df(df, x=df.index, y=df.Open, title='Daily Nikkei Open 1990-2019')
#plot_df_flipped_filled(df)
#plot_boxplots_yearwise(df)




###################################
### PATTERNS IN THE TIME SERIES ###
###################################

from statsmodels.tsa.seasonal import seasonal_decompose
from dateutil.parser import parse



def plot_multiplicative_additive_decomposition(df):
    #freq = 260
    freq = 500
    # Multiplicative Decomposition
    result_mul = seasonal_decompose(df['Open'], freq = freq, model='multiplicative', extrapolate_trend='freq')
    # Additive Decomposition
    result_add = seasonal_decompose(df['Open'], freq = freq, model='additive', extrapolate_trend='freq')
    # Plot
    plt.rcParams.update({'figure.figsize': (10,10)})
    result_mul.plot().suptitle('Multiplicative Decompose', fontsize=22)
    result_add.plot().suptitle('Additive Decompose', fontsize=22)
    plt.show()
    #plt.figure().savefig('test.png')


plot_multiplicative_additive_decomposition(df)

# https://en.wikipedia.org/wiki/Approximate_entropy
ss = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/sunspotarea.csv')
a10 = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/a10.csv')
rand_small = np.random.randint(0, 100, size=36)
rand_big = np.random.randint(0, 100, size=136)
easy = range(200)


def SampEn(U, m, r):
    """Compute Sample entropy"""
    def _maxdist(x_i, x_j):
        return max([abs(ua - va) for ua, va in zip(x_i, x_j)])

    def _phi(m):
        x = [[U[j] for j in range(i, i + m - 1 + 1)] for i in range(N - m + 1)]
        C = [len([1 for j in range(len(x)) if i != j and _maxdist(x[i], x[j]) <= r]) for i in range(len(x))]
        return sum(C)

    N = len(U)
    return -np.log(_phi(m+1) / _phi(m))

fig = plt.figure()
plt.plot(range(20), df.Open[-20:])
plt.title("Nikkei last month")
plt.xlabel("Time")
plt.ylabel("Value")
plt.show()

#print(SampEn(ss.value, m=2, r=0.2*np.std(ss.value)))      # 0.78
#print(SampEn(a10.value, m=2, r=0.2*np.std(a10.value)))    # 0.41
print('Sample Entropy for Nikkei:                   ' + str(SampEn(df.Open[-20:], m=2, r=0.2*np.std(df.Open[-20:]))))
print('Sample Entropy for linear Function:          ' + str(SampEn(easy, m=2, r=0.2*np.std(easy))))
#print(SampEn(rand_small, m=2, r=0.2*np.std(rand_small)))  # 1.79
#print(SampEn(rand_big, m=2, r=0.2*np.std(rand_big)))      # 2.42
