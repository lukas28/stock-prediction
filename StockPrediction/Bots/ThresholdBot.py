# This kind of bot uses the base functionality of the SimpleBot implementation.
# Other than the SimpleBot, the ThresholdBot needs the model input to be a numeric value rather 
# than a buy/sell signal. 
# Threshold value has to be provided in %

from SimpleBot import SimpleBot

class ThresholdBot(SimpleBot):

	def __init__(buyThreshold, sellThreshold):
		super().__init__()
		self.lastIndex = 0
		self.buyThreshold = buyThreshold
		self.sellThreshold = sellThreshold

	def trade(currentIndex, modelInput)
		if self.currentlyIn == False and (self.lastIndex * 100 / currentIndex) > buyThreshold:
			self.buy(currentIndex)
		if self.currentlyIn == True and (self.lastIndex * 100 / currentIndex) < sellThreshold:
			self.sell(currentIndex)
		self.lastIndex = currentIndex
