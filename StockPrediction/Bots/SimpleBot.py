# This simple bot gets the information from the model whether to buy or sell stocks.
# If the model suggests buying and the bot does not hold stocks --> buy with all the money
# If the model suggests selling and the bot does hold stocks --> sell all the stocks
# Depending on the success of the strategy, the ROI will rise or fall
# Output of the bot is the ROI (Return of invest) in %


class SimpleBot():

	def __init__(self):
		self.startCapital = 1000
		self.capital = self.startCapital
		self.currentlyIn = False
		self.boughtIndex = 0

	def trade(self,currentIndex, modelInput):
		if modelInput == 1 and self.currentlyIn == False:
			self.buy(currentIndex)
			return 1
		if modelInput == 0 and self.currentlyIn == True:
			self.sell(currentIndex)
			return 0
		return -1

	def buy(self, currentIndex):
		print('bought stocks for: ' + str(currentIndex))
		self.boughtIndex = currentIndex
		self.currentlyIn = True

	def sell(self,currentIndex):
		print('sold stocks for: ' + str(currentIndex))
		self.currentlyIn = False
		tradeFactor = (currentIndex * 100 / self.boughtIndex) / 100
		self.capital = self.capital * tradeFactor

	def getRoi(self):
		print('cash start: ' + str(self.startCapital) + 'cash end: ' + str(self.capital))
		return (self.capital * 100 / self.startCapital)
