import pandas as pd
import numpy as np
import keras
import tensorflow as tf
from keras.preprocessing.sequence import TimeseriesGenerator
import plotly
#mport plotly.plotly as py
import plotly.graph_objs as go
from keras.layers import Dropout
from keras import optimizers
from keras.models import Sequential
from keras.layers import LSTM, Dense, Activation
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error
import math


learning_rate = 0.001
momentum = 0.003
fse = 0.1
epochs = 1000

def load_data():
    filename = "/Users/lukashubl/Desktop/DJI.csv"
    df = pd.read_csv(filename)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_axis(df['Date'], inplace=True)
    df.drop(columns=['High', 'Low', 'Volume'], inplace=True)
    open_data = df['Open'].values
    close_data = df['Close'].values

    return open_data, close_data

def process_data(open_data, close_data):
    inputs = []
    outputs = []

    for i in range(len(open_data)):
        if i > 6:
            inputs.append(open_data[i-6:i])
            outputs.append(close_data[i])
            
    #scaler = StandardScaler()
    #scaler.fit(inputs)  # Don't cheat - fit only on training data
    #inputs = scaler.transform(inputs)
    #X_test = scaler.transform(X_test)  # apply same transformation to test data
    #normalize data


    return np.array(inputs), np.array(outputs)



def createModel1():
    model = Sequential()
    model.add(Dense(6))
    model.add(Activation('sigmoid'))
    model.add(Dense(10))
    model.add(Activation('sigmoid'))
    model.add(Dense(5))
    model.add(Activation('sigmoid'))
    model.add(Dense(1))

    opti = optimizers.SGD(lr=learning_rate, momentum = momentum)
    #opti = optimizers.RMSprop(lr=learning_rate)
    #opti = optimizers.Adam(lr = learning_rate)
    model.compile(optimizer=opti, loss='mse')
    return model


def main():
    open_data, close_data = load_data()
    inputs, outputs = process_data(open_data, close_data)
    model = createModel1()
    model.fit(x=inputs[:4000], y=outputs[:4000], epochs = epochs, verbose = 2, batch_size=64)
    prediction = model.predict(inputs[4001:4500])
    #plt.plot(prediction)
    #plt.plot(outputs[4001:4500])
    #plt.show()
    
    #lets calculate the root mean squared error
    testScore = math.sqrt(mean_squared_error(outputs[4001:4500], prediction))
    print('Test score: %.2f rmse', testScore)
    
    capital = 1000
    sell_price = 0
    buy_price = 0
    positive_trades = 0
    negative_trades = 0
    currently_bought = False
    for i in range(500):
        prediction = model.predict(inputs[4001+i:4002+i])
        if prediction > inputs[i + 4001][5] and currently_bought == False:
            #print("buy")
            currently_bought = True
            buy_price = inputs[i + 4001][5]
        elif prediction < inputs[i + 4001][5] and currently_bought == True:
            #print("sell")
            sell_price = inputs[i + 4001][5]
            print("bought for: " + str(buy_price) + " sold for: " + str(sell_price))
            currently_bought = False
            capital = capital * (sell_price * 100 / buy_price)/100
            if ((sell_price * 100 / buy_price)/100) > 1:
                positive_trades+=1
            if ((sell_price * 100 / buy_price)/100) < 1:
                negative_trades+=1
            
    print("capital: " + str(capital))
    print("positive trades: " + str(positive_trades))
    print("negative trades: " + str(negative_trades))
            
    


if __name__ == '__main__':
    main()
