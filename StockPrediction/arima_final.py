import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import lag_plot
from pandas import datetime
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
from Bots.SimpleBot import SimpleBot
from statsmodels.tsa.stattools import adfuller
from numpy import log
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from pmdarima.arima.utils import ndiffs
from statsmodels.tsa.arima_model import ARIMA
from Bots.SimpleBot import SimpleBot

from statsmodels.tsa.arima_model import ARIMA
import pmdarima as pm
import numpy as np

df = pd.read_csv("data/big/DJI.csv").fillna(0)

#df = df[-250:]
df = df.reset_index()
df.head()


def test_stationarity(timeseries, window = 12, cutoff = 0.01):

    #Determing rolling statistics
    rolmean = timeseries.rolling(window).mean()
    rolstd = timeseries.rolling(window).std()

    #Plot rolling statistics:
    fig = plt.figure(figsize=(12, 8))
    orig = plt.plot(timeseries, color='blue',label='Original')
    mean = plt.plot(rolmean, color='red', label='Rolling Mean')
    std = plt.plot(rolstd, color='black', label = 'Rolling Std')
    plt.legend(loc='best')
    plt.title('Rolling Mean & Standard Deviation')
    plt.show()

    #Perform Dickey-Fuller test:
    print('Results of Dickey-Fuller Test:')
    dftest = adfuller(timeseries, autolag='AIC', maxlag = 20 )
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic','p-value','#Lags Used','Number of Observations Used'])
    for key,value in dftest[4].items():
        dfoutput['Critical Value (%s)'%key] = value
    pvalue = dftest[1]
    if pvalue < cutoff:
        print('p-value = %.4f. The series is likely stationary.' % pvalue)
    else:
        print('p-value = %.4f. The series is likely non-stationary.' % pvalue)

    print(dfoutput)


test_stationarity(df['Open'])

first_diff = df.Open - df.Open.shift(1)
first_diff = first_diff.dropna(inplace = False)
#test_stationarity(first_diff, window = 12)


# PLOT THE TIME SERIES AND CHECK WHICH OF THEM SEEMS STATIONARY
# FOR ME THE FIRST DIFF LOOKS ALREADY PRETTY GOOD
# Original Series
'''fig, axes = plt.subplots(3, 2, sharex=True)
axes[0, 0].plot(df.Open); axes[0, 0].set_title('Original Series')
plot_acf(df.Open, ax=axes[0, 1])

# 1st Differencing
axes[1, 0].plot(df.Open.diff()); axes[1, 0].set_title('1st Order Differencing')
plot_acf(df.Open.diff().dropna(), ax=axes[1, 1])

# 2nd Differencing
axes[2, 0].plot(df.Open.diff().diff()); axes[2, 0].set_title('2nd Order Differencing')
plot_acf(df.Open.diff().diff().dropna(), ax=axes[2, 1])'''

#plt.show()

# USE THIS LIBRARY TO ESTIMATE THE NEEDED DEGREE OF DIFFERENTIATION
# THE LAST TO SUGGEST ALSO 1 WHICH CONFIRMS MY THOUGHTS

## Adf Test
print(ndiffs(df.Open, test='adf'))  # 0

# KPSS test
print(ndiffs(df.Open, test='kpss'))  # 1

# PP test:
print(ndiffs(df.Open, test='pp'))  # 1


# TRY TO FIND THE NEEDED P VALUE - THIS IS ACTUALLY PRETTY COMPLEX AND I CANT
# GET IT YET
# PACF plot of 1st differenced series
'''plt.rcParams.update({'figure.figsize':(9,3), 'figure.dpi':120})

fig, axes = plt.subplots(1, 2, sharex=True)
axes[0].plot(df.Open.diff()); axes[0].set_title('1st Differencing')
axes[1].set(ylim=(0,5))
plot_pacf(df.Open.diff().dropna(), ax=axes[1])

#plt.show()
# the result of this is that p should be 1 because it is the last one over
# blue area in the


# FIND OUT THE NUMBER OF MA TERMS (Q)
fig, axes = plt.subplots(1, 2, sharex=True)
axes[0].plot(df.Open.diff()); axes[0].set_title('1st Differencing')
axes[1].set(ylim=(0,1.2))
plot_acf(df.Open.diff().dropna(), ax=axes[1])

plt.show()'''
# q should also be 1 here






def makeForecast(train_data, test_data):
    train_data, test_data = df[0:int(len(df)*0.7)], df[int(len(df)*0.7):]
    test_data = test_data.reset_index()

    model_fit = pm.auto_arima(train_data['Open'], start_p=1, start_q=1,
                          test='adf',       # use adftest to find optimal 'd'
                          max_p=10, max_q=10, # maximum p and q
                          m=1,              # frequency of series
                          d=None,           # let model determine 'd'
                          seasonal=False,   # No Seasonality
                          start_P=0,
                          D=0,
                          trace=False,
                          error_action='ignore',
                          suppress_warnings=True,
                          stepwise=True)
    #print(model_fit.summary())

    # Actual vs Fitted
    output, confint = model_fit.predict(n_periods=len(test_data['Open']), return_conf_int=True)
    #plt.plot(output, label ='Predicted')
    #plt.plot(test_data['Open'][:10], label ='Real')
    #plt.show()

    rise = False
    real_rise = False

    if output[-1] > output[0]:
        #print("rise")
        rise = True
    #else:
        #print("fall")

    if test_data['Open'][:10].iloc[-1] > test_data['Open'][:10].iloc[0]:
        #print('real rise')
        real_rise = True
    #else:
        #print('real fall')

    if rise == real_rise:
        return True
    else:
        return False



'''
backup_df = df.copy(deep=True)
train_lags = [100, 50, 40, 30, 20, 10]
predict_lags = [10, 5, 4, 3, 2, 1]

log_file = 'ARIMA_TRAIN_PREDICT_GRID_SEARCH.txt'
best_success = 0

for train_lag in train_lags:
    for predict_lag in predict_lags:

        df = backup_df.copy(deep=True)
        successCounter = 0
        failureCounter = 0

        print('dataset length: ' + str(len(df.Open)))
        print('train lag: ' + str(train_lag))
        print('predict lag: ' + str(predict_lag))

        while len(df.Open) > (train_lag + predict_lag):
            train_data = df.Open.iloc[:train_lag]
            test_data = df.Open.iloc[train_lag:(train_lag+predict_lag)]
            test_data.reset_index()
            success = makeForecast(train_data, test_data)
            if success:
                successCounter += 1
            else:
                failureCounter += 1

            df.drop(df.index[0], inplace = True)

        f=open(log_file, "a+")
        f.write('train_lag: ' + str(train_lag) + ', predict_lag: ' + str(predict_lag) + ', dataset: 250 samples\n')
        f.write('Successful forecasts: ' + str(successCounter) + '\n')
        f.write('Failure forecasts: ' + str(failureCounter) + '\n')
        f.write('Success rate: ' + str(100*successCounter/(successCounter+failureCounter)) + '%\n')
        f.write('#################################################################\n')
        f.close()
        if 100*successCounter/(successCounter+failureCounter) > best_success:
            best_success = 100*successCounter/(successCounter+failureCounter)


print('best score: ' + str(best_success))
'''
