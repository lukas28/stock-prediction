# MACHINE LEARNING COURSE - FH HAGENBERG 2019/20

## Repository
This repository is used to share code, data and general information used for the machine learning course of the MCM studies first semester 2019/20.

## Project team
1. Dominik Grüneis (S1910455004)
2. Lukas Hubl (S1910455023)
3. Harald Pinsker (S1910455003)

# MACHINE LEARNING PROJECT

## IDEA DESCRIPTION: Stock Market Forecasting
The idea is to use a pair of time series variables to predict the stock prices. The two time series variables are on the one hand the stock prices and on the other hand the public`s mood states. In the paper of J. Bollen (https://arxiv.org/pdf/1010.3003.pdf) he already found out that there is a pretty strong correlation between the calmness of the public and the DJIA (Dow Jones Industrial Average) using Granger Causality (https://www.youtube.com/watch?v=ZUv7T8iPGrc).

## To Dos
1. Create code for mining data from the Twitter API which is perfectly suitable for this type of sentiment analysis as there are possibilities to query certain kinds of topics which might be especially suitable to find out people`s moods.
2. What has to be checked before, is the API request limits and whether there are accessible datasets which recorded useful Twitter data to circumvent possible API limitations.
3. Get access to DJIA or any other index we might consider in this project.

### Useful links
1. https://finance.yahoo.com/quote/%5EDJI/history?period1=1286488800&period2=1444255200&interval=1d&filter=history&frequency=1d

## Data
1. For sentiment analysis we use the Twitter API and might use additional Twitter Datasets available. Then the data is going to be filtered to make sure the tweets will give information about the emotions of the author using wordlists. Additionally the date when the tweet was released might be relevant.
2. For stock data purposes, we use the yahoo finance data which gives us the DJIA Date, Open value, High value, Low Value, Close value and volume of the day.

## Goals of the project
1. The main goal would be to be able to predict the stock closing price based on twitter sentiment analysis and stock opening price of that day in addition to a certain time window of stock prices.
2. If it turns out to be too hard to do this, our plan B would be to at least predict the trend of that day.
3. Another goal is to compare our prediction accuracy with the one of J. Bollen`s approach and thus point out how our most likely much simpler approach performs.

## Useful research links
1. https://www.toptal.com/python/twitter-data-mining-using-python
2. https://medium.com/@tomyuz/a-sentiment-analysis-approach-to-predicting-stock-returns-d5ca8b75a42
3. https://towardsdatascience.com/stock-prediction-using-twitter-e432b35e14bd
4. https://developer.twitter.com/en/docs/basics/rate-limiting
5. https://www.youtube.com/watch?v=ZUv7T8iPGrc
6. https://arxiv.org/pdf/1010.3003.pdf
7. https://pdfs.semanticscholar.org/4ecc/55e1c3ff1cee41f21e5b0a3b22c58d04c9d6.pdf
8. 


# New Approach
## Goal
The new goal of the project is to try as many different techniques as possible (grasp the internet for different approaches). We can than verify which
 of the approaches work best and determine the different problems and generally compare them. Moreover we should try these different trechniques (models) with 
 different input data namely DJI, Twitter?, Fiat money exchange rates, Oil prices and so on. As one step of evaluation we should also implement different 
 trading strategies based on trading bots which are able to intoduce different evaluation metrics upon the standard prediction accuracy.
 After implementing the above named steps, it pretty likely that we did not find a working solution. Therefore it is really important for our project to 
 provide a detailed expaination why it is so hard to predict the market (find and cite papers about the market impredictability). And what steps would be 
 required to enhance the results.
 
## Models/Techniques
1. Neural Network
2. LSTM
3. Decision Tree
4. Random Forest
5. Support Vector Machines
6. Logistic Regression
7. ARMA

## Datasets
1. DJI Timeseries
2. Oil Timeseries
3. Dollar/Pounds Exchange Timeseries
4. Twitter?

All of the timeseries data has to be considered in different window sizes, as mixtures, with additional feature extractions, ... It is very 
important to experiement here.

## Trading Strategies/Bots
1. Simple training bot which buys if not already bought and model predicts rise and sells if not already sold and model predicts decline. Always buys and sells with all money available.
2. Trading based on prediction rise/fall threshold should lower the transactions and thus the expenses for trading fees. Always using all the money.
3. Trading based on predicted likelyhood of rise/fall to occur. If the model is 90% sure that the stock is rising, trade 90% of the available money.

## Documentation
### Begin of the document
1. What is the project all about. 
2. What is the goal of the project (we should name the first intentions here as well). 
3. State how the requirements/goals changed over the working periods. 
4. Why did the initial approach fail and how do we try to come to a solution now (using other techniques and datasets istead of only lstm and dji+twitter).

### Models/Techniques
1. Whats the name of the technique.
2. What is the general idea of that specific technique/model
3. Cite papers of initial model invention (if accessible) and find references where this kind of model was used as well

### Datasets
1. What data is contained in the dataset (twitter, dji, exchange rate, mixture of differnet datasets, derived values, ...)
2. Where is the data from (link, citation, ...)
3. Generall data analysis as in the initial assignments (correlations, graphs, ...)
4. Why did we consider this dataset and what was our intention to use this kind of data for our mission

### Trading strategies / Bots
1. How does the bot trade, what is the strategy behind
2. What is the expacted input into the trading system (model says buy/sell, last 10 predictions, ...)
3. How does the bot trade (all in, thresholding, prediction confidence, ...)

### Implementations
This has to be done for each model/dataset combination
1. State which model was used for this appoach (or combination of multiple models) --> reference to the corresponding model section in the documentation
2. State which dataset was used (or combination of multiple datasets) --> reference to the corresponding dataset section in the documentation
3. What kind of output does the model deliver (value prediction, trand forecasting)
3. What did we expect to happen before implementation (very likely to work because xyz, not so much hope because xyz)
4. What scores did we get (accuracy, tading bot1, trading bot2, trading botN)

### Conclusion
1. Compare the differenct implementations. Where are the strengts and weaknesses in them. 
2. What is the general difficulty of predicting stock prices (cite paper on market impredictability)
3. Is there a way to improve the results (more data)
4. What are the bottle necks (too less data, too less computation power, market not predictable, ...)
5. What did we learn from this project.


### Work progress
<table>
  <thead>
    <tr>
      <th>Model</th>
      <th>Dataset</th>
      <th>Implemented</th>
      <th>Evaluated</th>
      <th>Documented</th>
      <th>Notes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Neural Network</td>
      <td>DJI + Oil + Exchange Sliding Window 5</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>[Notes](#nn-notes)</td>
    </tr>
    <tr>
      <td>RBFSVC</td>
      <td>DJI + Oil + Exchange Sliding Window 5</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
    </tr>
    <tr>
      <td>Linear SVC</td>
      <td>DJI + Oil + Exchange Sliding Window 5</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
    </tr>
    <tr>
      <td>Random Forest</td>
      <td>DJI + Oil + Exchange Sliding Window 5</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
    </tr>
    <tr>
      <td>Decision Tree</td>
      <td>DJI + Oil + Exchange Sliding Window 5</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
    </tr>
    <tr>
      <td>Logistic Regression</td>
      <td>DJI + Oil + Exchange Sliding Window 5</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
    </tr>
    <tr>
      <td>Nearest Neighbor</td>
      <td>DJI + Oil + Exchange Sliding Window 5</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
    </tr>
  </tbody>
</table>

### Progress notes
### Info to Neural Network (#nn-notes)
You can put some additional notes to the test config here


